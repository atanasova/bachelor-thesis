fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010001.gt.txt
Kreüter
|||||||
Kreüter

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010002.gt.txt
ner erſchein~ũg / vnſerer te~ütſcher
||||||||||||+#||||||||||||||+#||||||
ner erſcheinñ◌ͤg / vnſerer teůitſcher

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010003.gt.txt
zaun oder hagwurtzel / gar nicht /
-|||||||||||||||||||||||||||||||||
~aun oder hagwurtzel / gar nicht /

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010004.gt.txt
welche der mehrertheil balbierer
||||||||||||||#|||||||||||||||||
welche der mehnertheil balbierer

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010005.gt.txt
f~ü~r rechte Ariſtolochiam rotun⸗
|+|+|||||||||||||||||||||||||||||
ffüir rechte Ariſtolochiam rotun⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010006.gt.txt
dam einſamlend. Dioſc. Diſer
||||||||||||||||||||||||||||
dam einſamlend. Dioſc. Diſer

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010007.gt.txt
wurtzel etwas mit wein myrrhen
||||||||||||||||||||||||||-|||
wurtzel etwas mit wein myr~hen

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010008.gt.txt
vnd pfeffer getruncken / reiniget
|||||||||||||||||||||||||||||||||
vnd pfeffer getruncken / reiniget

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010009.gt.txt
die weiber von vberflißigem vn⸗
||||||||||||||||||||-||||||||||
die weiber von vberf~ißigem vn⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000a.gt.txt
rath der m~ůter / treibt auß die an
||||||||||+#|||||||||||||||||||||||
rath der mu◌ͤter / treibt auß die an

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000b.gt.txt
d◌̉ geburt vñ weiber menſes. Ein
|-|||||||||||||||||||||||||||||
d~ geburt vñ weiber menſes. Ein

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000c.gt.txt
ſalb gemacht vonn diſer wurtzen
|||||||||||||||||||||||||||||||
ſalb gemacht vonn diſer wurtzen

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000d.gt.txt
zeitloſen vñ anagallide ze~ücht vß
||||||||||||||||||||||||||+#||||||
zeitloſen vñ anagallide zeůicht vß

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000e.gt.txt
ſpreiſſel / do◌ͤrn vñ~ geſchiferte bein.
||#||||||||||-|#||||+||||||||||||||||||
ſpteiſſel / d~◌ͤin vñi geſchiferte bein.

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/01000f.gt.txt
~Hiemitt beſchlies ich mein
+#|||||||||||||||||||||||||
Odiemitt beſchlies ich mein

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010010.gt.txt
rede diſer zeit von den zwelff zei⸗
|||||||||||||||||||||||||||||||||||
rede diſer zeit von den zwelff zei⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010011.gt.txt
chen kreü~tteren / bega◌ͤren menck⸗
|||||||||+||||||||||||||||||||||||
chen kreüitteren / bega◌ͤren menck⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010012.gt.txt
lich welle mirs im beſten aufn~em
||||||||||||||||||||||||||||||+||
lich welle mirs im beſten aufn em

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010013.gt.txt
men als dañ ichs gethan / hab ſye
|||||||||||||||||||||||||||||||#|
men als dañ ichs gethan / hab ſpe

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010014.gt.txt
weitleü~ffiger beſchribẽ wellen / ſo
|||||||+||||||||||||-|||||||||||||||
weitleüiffiger beſch~ibẽ wellen / ſo

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010015.gt.txt
ſind yetzi~ger k~ürtze viel vrſach~en /
|||||-||||+|||||+#|||||||||||#||||+||||
ſind ~etzi ger kiirtze viel vtſach en /

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010016.gt.txt
vorauß dieweil ich gro~ſſen k~oſt~en
||||||||||||||||||||||+||||||+|||+||
vorauß dieweil ich gro ſſen k oſt en

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010017.gt.txt
angewendet in ſůch~ung der ~kre~ü
|||||||||||||||#||+||||||||+#||+#
angewendet in ſuch ung der r reii

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010018.gt.txt
ter auß eignem willen vñ be~üt~tel /
|||||||||||||||||||||||#|||+#|+|||||
ter auß eignem willen vi beiit tel /

fraktur1/1557-WieSichMeniglich-Bodenstein/0036/010019.gt.txt
~~nam⸗
++###|
edrnt⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010001.gt.txt
Beſchreibung xlvi
|||||#|||||||#|||
Beſchneibung rlvi

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010002.gt.txt
namlichen deren / ſo nicht alle tag
|||||||||||||||||||||||||||||||||||
namlichen deren / ſo nicht alle tag

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010003.gt.txt
gefunden mo◌ͤgendt werden oder
||||||||||#||||||||||||||||||
gefunden m ◌ͤgendt werden oder

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010004.gt.txt
yedem ſch~weinhirten kund ſind.
|||||||||+|||||||||||||||||||||
yedem ſch weinhirten kund ſind.

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010005.gt.txt
Aber mittler zeitt ſich grobe leüt
||||||||||||||||||||||||||||||||#|
Aber mittler zeitt ſich grobe leit

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010006.gt.txt
bey mir verfu◌ͤget / mehr als ich
||||||||||||||||||||||||||||||||
bey mir verfu◌ͤget / mehr als ich

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010007.gt.txt
winſthe / meiner geh~eimnuß der
||||||||||||||||||||+||||||||||
winſthe / meiner geh eimnuß der

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010008.gt.txt
kreü~ter rau~ſſer klaubet / Ja nach
||||+|||||||+||||||||||||||||||||||
kreüiter rau ſſer klaubet / Ja nach

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010009.gt.txt
jrer gewonten ſchmeichleriſchen
|||||||||||||||||||||||||||||||
jrer gewonten ſchmeichleriſchen

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000a.gt.txt
art mit langſamer gleißneriſcher
||||||||||||||||||||||||||||||||
art mit langſamer gleißneriſcher

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000b.gt.txt
rede ein weil mich beſůcht / zu letſt
||||||||||||||||||||||#||||||||||||||
rede ein weil mich beſucht / zu letſt

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000c.gt.txt
mit dem vntre~üwẽ ſchenckel (~wie
|||||||||||||+#||||||||||||||+|||
mit dem vntreiiwẽ ſchenckel ( wie

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000d.gt.txt
das ſprich~wort iſt) dra◌ͤttẽ wellen.
||||||#|||+||#|-||||||#||||-||||||||
das ſptich woit~iſt) dta◌ͤtt~ wellen.

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000e.gt.txt
Daha◌ͤr ich nur mehr im zweifel ge
||||||||||||||||||--|||||||||||||
Daha◌ͤr ich nur meh~~im zweifel ge

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/01000f.gt.txt
wa◌ͤſt was zeth~ůn were / vñ wo die
||||||||||||||+#||||||||||||||||||
wa◌ͤſt was zethu◌ͤn were / vñ wo die

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010010.gt.txt
arbeit nicht gereit angangẽ / deß
||||||||||#||||||||||||||||||||||
arbeit nicbt gereit angangẽ / deß

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010011.gt.txt
gleichen from~mer ehren leütẽ an
|||||||||||||+|||||##|||||||#|||
gleichen from mer ebten leüte an

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010012.gt.txt
manen denen diß zeſchriben zůge
|||||||||||||||||||||-||||||#||
manen denen diß zeſch~iben zuge

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010013.gt.txt
ſagt gewa◌ͤſt / hette ichs vn~derge⸗
||||||||||||||||||||||||||||+||||||
ſagt gewa◌ͤſt / hette ichs vn derge⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010014.gt.txt
ſchlagen. Derhalben fr~üntlicher
||||||||||||||||||||||+#||||||||
ſchlagen. Derhalben friintlicher

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010015.gt.txt
la◌ͤſer laß dich auff diß m~al vernu◌ͤ⸗
||||||||||||||#|||||||||||+||||||||||
la◌ͤſer laß dicb auff diß m al vernu◌ͤ⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010016.gt.txt
gen / vñ~ was du guthertzig fr~ünt⸗
||||||||+|||||||||||||||||||||+#|||
gen / vñi was du guthertzig friint⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010017.gt.txt
lich / welches vn~ns chriſt~en durch
|||||||||||||||||+|||||-|||+||||||||
lich / welches vn ns ch~iſtten durch

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010018.gt.txt
einan~dern z~ůſthet beſſeren kanſt /
|||||+||||||+#||||||||||||||||||||||
einan dern zu◌ͤſthet beſſeren kanſt /

fraktur1/1557-WieSichMeniglich-Bodenstein/0037/010019.gt.txt
E ij daß
--|-||||
~~i~ daß

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010001.gt.txt
Kreü~ter
||||+|||
Kreüiter

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010002.gt.txt
dz beſſere / deß vberig vrteile nicht
|||||||||||||||||||||||||#|||||-||||#
dz beſſere / deß vberig vnteile~niche

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010003.gt.txt
auß neid / laß in ſeiner würde blei⸗
||||||||||-|||||||||||||||#|||||||||
auß neid /~laß in ſeiner wůrde blei⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010004.gt.txt
ben / ſthet fr~üntlich mir v~mb dich
||||||||||||||+#||||||||||||+|||||||
ben / ſthet friintlich mir v mb dich

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010005.gt.txt
zebeſchulden / vnd ſchier kü~nfftig
||||||||||||||||||||||||||||+||||||
zebeſchulden / vnd ſchier küinfftig

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010006.gt.txt
etwas weiters zeo◌ͤffnen.
||||||||||||||||-|||||||
etwas weiters ze~◌ͤffnen.

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010007.gt.txt
Sich g~ünſtiger lieber la◌ͤſer / im
||||||+#|||||||||||||||||||||||-||
Sich giinſtiger lieber la◌ͤſer /~im

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010008.gt.txt
punctẽ als diſes bu◌ͤch~lein vollen⸗
||||||||||||||||||#|||+||||||||||||
punctẽ als diſes bů◌ͤch lein vollen⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010009.gt.txt
det / kompt ein fleiſſiger wurtzen⸗
|||||||||||||||||#|||||||||||||||||
det / kompt ein fteiſſiger wurtzen⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000a.gt.txt
graber / bringt mir v~õ bergen ein
||||||||||#||||||||||+#|||||||||||
graber / btingt mir voo bergen ein

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000b.gt.txt
ſcho◌ͤns hibſchs gewa◌ͤchs / ein feins
|||--|||||||||||||||||||||||||||||||
ſch~~ns hibſchs gewa◌ͤchs / ein feins

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000c.gt.txt
ſchottechti~gs ſteüdlein mitt tun⸗
|||||||||||+||||||#||||||||||||||#
ſchottechti gs ſteidlein mitt tuno

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000d.gt.txt
ckelgru◌ͤnen hip~ſchen ſpitzen blett⸗
||||||||||||#||+||#|||||||||||||||||
ckelgru◌ͤnen bip ſcben ſpitzen blett⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000e.gt.txt
leinen gar nach an~zu~ſchaen~ als
||||||||||||||||||+||+||||||+||||
leinen gar nach an zunſchaenr als

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/01000f.gt.txt
Hyſſop / ſein ſtẽgel iſt etwz braun /
##||||||||||||||-|||-||||||||||#|||||
dpſſop / ſein ſt~gel~iſt etwz btaun /

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010010.gt.txt
tregt zwiſchen den blettlin hi~m◌̃el
||||||||||||||||||||||||||||||+##||
tregt zwiſchen den blettlin hiññiel

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010011.gt.txt
blae blu◌ͤm~lin / vñ in yetlichẽ ſtrich
||||||||||+||||||||#|||#||||||-|||||||
blae blu◌ͤm lin / vñiin zetlich~ ſtrich

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010012.gt.txt
lin wie ſternli / hats in mitte ſcho◌ͤn~
||||||||||||||||--||||||||||||||||||||+
lin wie ſternli ~~hats in mitte ſcho◌ͤnn

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010013.gt.txt
goldtgelb w~ürtzelein. Wolt gar
|||||||||||+#||||||||||||||||||
goldtgelb wiirtzelein. Wolt gar

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010014.gt.txt
gern wiſſen / wie es ein verſtendi⸗
|||||||||||||||||||||||||||||||||||
gern wiſſen / wie es ein verſtendi⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010015.gt.txt
ger nam~b~ſen da◌ͤtt / d~ann darauß
|||||||+|+|||||||||||||+||||||||||
ger nam b ſen da◌ͤtt / d ann darauß

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010016.gt.txt
vielich~ter ſein frucht volgete. Es
|||||||+|||||||||||||||||||||||||||
vielich ter ſein frucht volgete. Es

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010017.gt.txt
hats etwar d~och nicht vß der zaal
||||||||||||+|||||||||||||||||||||
hats etwar d och nicht vß der zaal

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010018.gt.txt
der gar h~och~gelerten / wiewol er
|||||||||+|||+||||||||||||||||||||
der gar h och gelerten / wiewol er

fraktur1/1557-WieSichMeniglich-Bodenstein/0038/010019.gt.txt
ſich
|||-
ſic~

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010001.gt.txt
Beſchreibung. xlvij
|||||#||||||||#|||-
Beſchieibung. rlvi~

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010002.gt.txt
ſich der ſelbigẽ einer berett zů ſein
|||||||||||||||||||||||||||||||#|||||
ſich der ſelbigẽ einer berett zi ſein

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010003.gt.txt
vrſach / dz er Theologiſchẽ ſtands
|#||||||-||||||#||||||||||-#|||||#
viſach /~dz er Cheologiſch~◌ͤſtanda

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010004.gt.txt
iſt / geſehen diſer gibt ſein vrtheil
||||||||||#||||||||||||||||||||#|||||
iſt / geſeben diſer gibt ſein vitheil

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010005.gt.txt
vñ~es ſey das recht Aſterion / oder
||+||||||||||||||||||||||||||||||||
vñ es ſey das recht Aſterion / oder

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010006.gt.txt
Solſequiũ / oder die Lunaria ſo
||||||||#||||||||||||-|||||||||
Solſequiß / oder die ~unaria ſo

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010007.gt.txt
doctor Ad~am ſ~ůchen laſſen / wel⸗
|||||#|||+||||+#||||||||||||||||||
docton Ad am ſu◌ͤchen laſſen / wel⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010008.gt.txt
le ſey auch wiſſen z~ůr Alchimey ze
||||||||||||||||||||+#|||||||||||||
le ſey auch wiſſen zu◌ͤr Alchimey ze

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010009.gt.txt
brauchen / Aber als er nicht z~ů vil
|#||||||||||||||||||||||||||||+#||||
btauchen / Aber als er nicht zu◌ͤ vil

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000a.gt.txt
hohe ding ergrindet hatt / darff
||||||||||||||||||||||||||||||||
hohe ding ergrindet hatt / darff

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000b.gt.txt
ich jm hierinn nicht glauben ge⸗
|||||||||||||||||||||||||||||||#
ich jm hierinn nicht glauben gee

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000c.gt.txt
ben. Was ſonſt m~ein perſon bela⸗
||||||||||||||||+||||||||||||||||
ben. Was ſonſt m ein perſon bela⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000d.gt.txt
det / der Lunariæ halben / do th~ůt
|||-#|||||#||||-#|||||||||-|||||+#|
det~, der Cunar~t halben /~do thu◌ͤt

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000e.gt.txt
er mir gwalt / dz ich ſye ye ein mal
||||||||||||||-||||||||#||||||||||||
er mir gwalt /~dz ich ſpe ye ein mal

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/01000f.gt.txt
ſ~ůchen laſſen. Gebendt dann an⸗
|+#||||||||||||-#|||||||||||||||
ſu◌ͤchen laſſen.~Bebendt dann an⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010010.gt.txt
der le~üt ohne meine beſtetigung
||||||+#||||||||||||||||||||||||
der leiit ohne meine beſtetigung

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010011.gt.txt
von kre~~ütern nam~m~en oder wür
|||||||++#||||||||+|+|||||||||||
von kreii tern nam m en oder wür

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010012.gt.txt
ckun~ge vß / das laſſe ich ſye verant
||||+|||||||||||||||||||||||#||||||||
ckun ge vß / das laſſe ich ſpe verant

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010013.gt.txt
worten / diew~eil ich keinen befelch
||#||||||||||+||||||||||||||||||||||
wonten / diew eil ich keinen befelch

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010014.gt.txt
hab yemãts rech~tzefertigẽ. Was
||||-||#|||||||+|||||||||-|||||
hab ~em◌ͤts rech tzefertig~. Was

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010015.gt.txt
ich von kreütern halt in mach~ũg
|||||||||||||||||||||||||||||+#|
ich von kreütern halt in machfig

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010016.gt.txt
der metallen / haſt geho◌ͤret do ich
|||||||||||||||||||||||#|#|||||||||
der metallen / haſt geh ◌ͤtet do ich

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010017.gt.txt
von Anagallide geredt. Luna⸗
#||||||||||||||||||||||#||||
won Anagallide geredt. Cuna⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010018.gt.txt
riae mitt einem eintzigen ſten~gel
||-|||||||||||||||||||||||||||+|||
ri~e mitt einem eintzigen ſten gel

fraktur1/1557-WieSichMeniglich-Bodenstein/0039/010019.gt.txt
G iij vnd
--||-||||
~~ii~ vnd

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010001.gt.txt
Kreüter
|||||||
Kreüter

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010002.gt.txt
vñ zerkerfften blatt / welches wur
||||||||||||||||||||||||||||||||||
vñ zerkerfften blatt / welches wur

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010003.gt.txt
tzel zaſerecht vnnd oben am ſten⸗
|||||||||||||||||||||||||||||||||
tzel zaſerecht vnnd oben am ſten⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010004.gt.txt
gel kleinen ſamen tregt / von der
|||||||||||||||||||||||||||||||||
gel kleinen ſamen tregt / von der

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010005.gt.txt
geſagt wirdt / ſolle den Alchimi⸗
|||||||||||||||||||||||||||||||||
geſagt wirdt / ſolle den Alchimi⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010006.gt.txt
ſten dienen / hab ich vor xvj jaren
||||||||||||||||||||||||#|#|#||||||
ſten dienen / hab ich vot zvſ jaren

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010007.gt.txt
gewiſt gn~ůgſam auff der waſſer⸗
|||||||||+#|||||||||||||||||||||
gewiſt gnu◌ͤgſam auff der waſſer⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010008.gt.txt
fallen zefinden. Das ander Lu⸗
||||||||||||||||-||||||||||#||
fallen zefinden.~Das ander Cu⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010009.gt.txt
nariam~ ſo ettwan ach~t~vnd~zwen⸗
|||#||+||||||||||||||+|+|||+|||||
nartamn ſo ettwan ach t vnd zwen⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000a.gt.txt
tzig bletter hat / wiſt ich im m~ooß
||||||||||||||||||||||||||||||||+|||
tzig bletter hat / wiſt ich im m ooß

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000b.gt.txt
bey ~Michelfeld~en vmb Baſel ze⸗
||#|+#|||||||||+||||||||||||||||
bep eDichelfeld en vmb Baſel ze⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000c.gt.txt
bekommen: Vn~d hab meine tag
||||||||#|||+|||||||||||||||
bekommen; Vnnd hab meine tag

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000d.gt.txt
nie kein kraut will geſchweigen
|||||||||||||||||||||||||||||||
nie kein kraut will geſchweigen

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000e.gt.txt
Lunariam geſůcht / das ich nur
#|||||||||||#|||||||||||||||||
Cunariam geſucht / das ich nur

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/01000f.gt.txt
verwa◌ͤndt viel minder gehoffet /
||||||||||||||||||||||||||||||||
verwa◌ͤndt viel minder gehoffet /

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010010.gt.txt
mann mo◌ͤchte einigerley metall
||||||||||||||||||||||#|||||||
mann mo◌ͤchte einigerlez metall

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010011.gt.txt
dardurch machen / darumb ſtehe
||||||||||||||||||||||||||||||
dardurch machen / darumb ſtehe

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010012.gt.txt
diſer ab von ſeiner hitz m~ir bo◌ͤſes
||||||||||||||||||||#|||||+||||-||||
diſer ab von ſeiner bitz m ir b~◌ͤſes

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010013.gt.txt
vmb gůtes zethůn derhalben be⸗
|||||#||||||||#|||||||||||||||
vmb gutes zethun derhalben be⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010014.gt.txt
weget / dz ich ſeiner gemeinſchafft
|||||||-||||||||||||||||||||||||||#
weget /~dz ich ſeiner gemeinſchaffr

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010015.gt.txt
m~ich nimm~~er vnder nem~⸗
|+||||||||++||||||||||||+|
mzich nimmn er vnder nemz⸗

fraktur1/1557-WieSichMeniglich-Bodenstein/0040/010016.gt.txt
men will.
|||||||||
men will.

