fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010001.gt.txt
daher es auch dem Magen ſehr angenehm iſt und ihn ſta◌ͤr⸗
||||||||||||||||#|||||||||||||||||||||||||||||||||||-|||
daher es auch dei Magen ſehr angenehm iſt und ihn ſt~◌ͤr⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010002.gt.txt
ket; iſt trefflich wider den Scharbock, wider Engbru◌ͤſtig⸗
||||||||||||||||||||#|||||||||||||||||||||||||||||||||||||
ket; iſt trefflich wlder den Scharbock, wider Engbru◌ͤſtig⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010003.gt.txt
keit und Milzbeſchwerden, verſu◌ͤßt das ſaure, ſalzige Ge⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||
keit und Milzbeſchwerden, verſu◌ͤßt das ſaure, ſalzige Ge⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010004.gt.txt
blu◌ͤt, widerſteht der Fa◌ͤule und verdu◌ͤnnet die za◌ͤhen Feuch⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
blu◌ͤt, widerſteht der Fa◌ͤule und verdu◌ͤnnet die za◌ͤhen Feuch⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010005.gt.txt
tigkeiten. Die Bla◌ͤtter und Wurzeln davon gedo◌ͤrrt und
|||||||||||||||||-|||||||||||||||||||||||||||-#|||||||
tigkeiten. Die Bl~◌ͤtter und Wurzeln davon ged~brrt und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010006.gt.txt
gepulvert dem Vieh, beſonders aber den Schafen, die mit
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
gepulvert dem Vieh, beſonders aber den Schafen, die mit

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010007.gt.txt
dem Huſten behaftet ſind, eingegeben, vertreiben denſelben
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
dem Huſten behaftet ſind, eingegeben, vertreiben denſelben

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010008.gt.txt
in kurzem.
||||||||||
in kurzem.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010009.gt.txt
Biberkra~ut, (ſiehe Tauſendguldenkraut.)
###|||||+||||||||||||||||||||||||||||||-
lb erkra ut, (ſiehe Tauſendguldenkraut.~

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000a.gt.txt
B~ilſe~n~kr~a~ut, S~a~u~bohnen, Sch~la~fkra~u~t.
|+||||+|+||+|+|||||+|+|+|||||||||||+#|+||||+|+||
B ilſe n kr a ut, S a u bohnen, Sch La fkra u t.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000b.gt.txt
Es giebt ein ſch~w~arzes und ein w~eißes Bilſe~nkr~a~u~t.
||||||||||||||||+|+|||||||||||||||+|||||||||||+|||+|+|+||
Es giebt ein ſch w arzes und ein w eißes Bilſe nkr a u t.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000c.gt.txt
Das ſch~w~a~rze oder ge~m~ei~ne Bilſenkr~au~t, welches
||||#||+|+|+|||||||||||+|+||+|||||||||||+||+||||||||||
Das fch w a rze oder ge m ei ne Bilſenkr au t, welches

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000d.gt.txt
wild an ungebauten Orten wa◌ͤchst, hat ſehr dicke und u◌ͤber
||||||||||||||||||||||||||||||||||#|||||||||||||||||||||||
wild an ungebauten Orten wa◌ͤchst, bat ſehr dicke und u◌ͤber

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000e.gt.txt
anderthalb Schuh hohe Stengel, ſchwarzgru◌ͤne, breite,
||||||||||||||||||||||||||||||||||||||||||||||||||||#
anderthalb Schuh hohe Stengel, ſchwarzgru◌ͤne, breite.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01000f.gt.txt
lange, weiche, wollichte, zerſchnittene und zerſpaltene
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
lange, weiche, wollichte, zerſchnittene und zerſpaltene

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010010.gt.txt
Bla◌ͤtter, welche faſt dem Fichtenlaub gleichen, aber weiter
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Bla◌ͤtter, welche faſt dem Fichtenlaub gleichen, aber weiter

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010011.gt.txt
am Stengel hinauf ſchma◌ͤler, kleiner und ſpitziger werden.
|||||||||||||||||||||||||||||||||||||||||||||-||||||||||||
am Stengel hinauf ſchma◌ͤler, kleiner und ſpit~iger werden.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010012.gt.txt
Seine Blumen, welche in bleichgelben, mit Purpur durch⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
Seine Blumen, welche in bleichgelben, mit Purpur durch⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010013.gt.txt
ſtrichenen, und in fu◌ͤnf Theile zerſpaltenen Glo◌ͤckchen be⸗
|||||||||||||||||||||||||||||||||||||||||||||||-#||||||||||
ſtrichenen, und in fu◌ͤnf Theile zerſpaltenen Gl~bckchen be⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010014.gt.txt
ſtehen, wachſen dicht beiſam~men, und haben inwendig vier
||||||||||||||||||||||||||||+||||||||||||||||||||||||||||
ſtehen, wachſen dicht beiſam men, und haben inwendig vier

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010015.gt.txt
oder fu◌ͤnf purpurfarbene Kno◌ͤpfe. Nach den Blumen folgt
|||||||||||||||||||||||||||-#||||||||||||||||||||||||||
oder fu◌ͤnf purpurfarbene Kn~bpfe. Nach den Blumen folgt

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010016.gt.txt
eine Frucht, die der La◌ͤnge nach zwei Fa◌ͤcher hat, worin
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
eine Frucht, die der La◌ͤnge nach zwei Fa◌ͤcher hat, worin

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010017.gt.txt
ein grauer Saame, dem Maiſaamen a◌ͤhnlich, verſchloſſen
||||||||||||#|||||||||||||||||||||||||||||||||||||||||
ein grauer Sname, dem Maiſaamen a◌ͤhnlich, verſchloſſen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010018.gt.txt
liegt. Die Wurzel iſt gelb, Fingers dick, und mit vielen
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
liegt. Die Wurzel iſt gelb, Fingers dick, und mit vielen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010019.gt.txt
Faſern bewachſen. Das ganze Gewa◌ͤchs hat einen wider⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||
Faſern bewachſen. Das ganze Gewa◌ͤchs hat einen wider⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001a.gt.txt
lich ſtinkenden Geruch, den beſonders die Ratten und
||||||||||||||||||||||||||||||||||||||||||||||||||||
lich ſtinkenden Geruch, den beſonders die Ratten und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001b.gt.txt
Ma◌ͤuſe nicht vertragen ko◌ͤnnen, daher es von den Bauern
||||||||||||||||||||||||-#|||#|||||||||||||||||||||||||
Ma◌ͤuſe nicht vertragen k~bnneu, daher es von den Bauern

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001c.gt.txt
um Johannis ausgegraben und in die Ha◌ͤuſer umhergelegt
||||||||||||||||||||||||||||||||#|||||||||||||||||||||
um Johannis ausgegraben und in dle Ha◌ͤuſer umhergelegt

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001d.gt.txt
wird. Das w~eiß~~e Bil~ſen~kraut iſt von dem ſch~wa~r⸗
|||||||||||+|||++|||||+|||+|||||||||||||||||||||+||+||
wird. Das w eißz e Bil ſen kraut iſt von dem ſch wa r⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001e.gt.txt
zen nur darin unterſchieden, daß es kleinere, weichere und
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
zen nur darin unterſchieden, daß es kleinere, weichere und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/01001f.gt.txt
weißere Bla◌ͤtter, weiße Blu~men und einen weißen Saamen
|||||||||||#|||||||||||||||+|||||||||||||||||||||||||||
weißere Blaätter, weiße Bluimen und einen weißen Saamen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010020.gt.txt
hat, ſonſt hat es einerlei Eigen~ſchaften mit dem ſchwarzen,
||||||||||||||||||||||||||||||||+||||||||||||||||||||||||||#
hat, ſonſt hat es einerlei Eigen ſchaften mit dem ſchwarzen.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010021.gt.txt
wiewohl das weiße zur Arzenei ſicherer als das ſchwarze
||||||||||||||||||||||||||||||||||||||||||||||||||||||#
wiewohl das weiße zur Arzenei ſicherer als das ſchwarzs

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010022.gt.txt
gebraucht werden kann.
||||||||||||||||||||||
gebraucht werden kann.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010023.gt.txt
Ge~b~ra~u~ch. Das Bilſenkraut mit ſeinen Blumen
||+|+||+|+|||||||||||||||||||||||||||||||||||||
Ge b ra u ch. Das Bilſenkraut mit ſeinen Blumen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0018/010024.gt.txt
und Saamen ſoll nicht ohne beſondere Vorſicht genommen
||||||||||||||||||||||#|||||||||||||||||||||||||||||||
und Saamen ſoll nicht vhne beſondere Vorſicht genommen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010001.gt.txt
werden, da es nicht nur den Menſchen, ſondern auch dem
||||||||||||||||||||||||||||||||||||||||||||||||||||||
werden, da es nicht nur den Menſchen, ſondern auch dem

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010002.gt.txt
Vieh ſcha◌ͤdlich, ja bisweilen to◌ͤdtlich iſt. Dieß kann man
|||||||||||||||||||||||||||||||-#|||||||||||||||||||||||||
Vieh ſcha◌ͤdlich, ja bisweilen t~bdtlich iſt. Dieß kann man

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010003.gt.txt
an den Fiſchen im Waſſer bemerken; denn wenn man Bil⸗
|||||||||||||||||||||||||||||||||||||#|||||||||||||||
an den Fiſchen im Waſſer bemerken; deun wenn man Bil⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010004.gt.txt
ſenſaamen mit Teig vermiſcht, oder nur in's Waſſer wirft,
|||||||||||||||||||||||||||||||||||||||||-|||||||||||||||
ſenſaamen mit Teig vermiſcht, oder nur in~s Waſſer wirft,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010005.gt.txt
fangen die Fiſche an zu toben, ſpringen auf und kehren
||||||||||||||||||||||||||||||||||||||||||||||||||||||
fangen die Fiſche an zu toben, ſpringen auf und kehren

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010006.gt.txt
zuletzt den Bauch u◌ͤber ſich, ſo; daß man ſie mit den Ha◌ͤn⸗
||||||||||||||||||||||||||||||||#||||||||||||||||||||||||||
zuletzt den Bauch u◌ͤber ſich, ſo/ daß man ſie mit den Ha◌ͤn⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010007.gt.txt
den fangen kann. Die Hu◌ͤhner auf den Balken fallen her⸗
||||||#||||||||||||||||||||||||||||||||||||||||||||||||
den faugen kann. Die Hu◌ͤhner auf den Balken fallen her⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010008.gt.txt
ab, wenn ſie mit Bilſenſaamen bera◌ͤuchert werden, und
|||||||||||||||||||||||||||||||||||||||||||||||||||||
ab, wenn ſie mit Bilſenſaamen bera◌ͤuchert werden, und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010009.gt.txt
wenn ſie den Saamen eſſen, ſo ſterben ſie davon. Auch
|||||||||||||||||||||||||||||||||||||||||||||||||||||
wenn ſie den Saamen eſſen, ſo ſterben ſie davon. Auch

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000a.gt.txt
die Menſchen, wenn ſie es innerlich gebrauchen, macht es
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
die Menſchen, wenn ſie es innerlich gebrauchen, macht es

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000b.gt.txt
toll und unſinnig, und bewirket einen unaufho◌ͤrlichen Schlaf.
|||||||||||||||||#||||||||||||||||||||||||||-#|||||||||||||||
toll und unſinnig. und bewirket einen unaufh~brlichen Schlaf.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000c.gt.txt
Wer fu◌ͤrchtet, Bilſenkraut oder Bilſenſaamen gegeſſen zu
||||||||||||||||||||||||#|||||||||||||||||||||||||||||||
Wer fu◌ͤrchtet, Bilſenkrant oder Bilſenſaamen gegeſſen zu

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000d.gt.txt
haben, der trinke ſogleich Ziegenmilch, oder nehme Knob⸗
|||||||||||||||||||||||||||||||||||||||||||||#||||||||||
haben, der trinke ſogleich Ziegenmilch, oder uehme Knob⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000e.gt.txt
lauch oder Zwiebeln zu ſich mit Wein. Wenn die wilden
|||||||||||||||||||||||||||||||||||||||||||||||||||||
lauch oder Zwiebeln zu ſich mit Wein. Wenn die wilden

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01000f.gt.txt
Schweine vom Bilſenkraut freſſen, bekommen ſie heftige
||||||||||||||||||#|||||||||##||||||||||||||||||||||||
Schweine vom Bilſeukraut freffen, bekommen ſie heftige

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010010.gt.txt
Zuckungen, woran ſie in kurzer Zeit ſterben mu◌ͤſſen, wenn
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Zuckungen, woran ſie in kurzer Zeit ſterben mu◌ͤſſen, wenn

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010011.gt.txt
ſie nicht zu einem Waſſer kommen, daraus ſaufen und ſich
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
ſie nicht zu einem Waſſer kommen, daraus ſaufen und ſich

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010012.gt.txt
darin baden ko◌ͤnnen. Man braucht alſo dieſes Kraut und
||||||||||||--#|||||||||||||||||||||||||||||||||||||||
darin baden ~~bnnen. Man braucht alſo dieſes Kraut und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010013.gt.txt
zwar meiſtens das weiße, zu Umſchla◌ͤgen, zu Salben,
|||||||||||||||||||||||||||||||||||||||||||||||||||
zwar meiſtens das weiße, zu Umſchla◌ͤgen, zu Salben,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010014.gt.txt
Pflaſtern und in Oele. Die friſchen Bla◌ͤtter von dieſem
|||||||||||||||||#|||||||||||||||||||||||||||||||||||||
Pflaſtern und in Dele. Die friſchen Bla◌ͤtter von dieſem

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010015.gt.txt
Kraute u◌ͤber alte Geſchwu◌ͤre und ſchmerzhafte oder geſchwol⸗
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Kraute u◌ͤber alte Geſchwu◌ͤre und ſchmerzhafte oder geſchwol⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010016.gt.txt
lene Glieder gelegt, benehmen die Schmerzen. Eine Salbe
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
lene Glieder gelegt, benehmen die Schmerzen. Eine Salbe

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010017.gt.txt
von dem Saamen, Eierklar, Frauenmilch und ein wenig
|||||||||||||||||||||||||||||||||||||||||||||||||||
von dem Saamen, Eierklar, Frauenmilch und ein wenig

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010018.gt.txt
Eſſig gemacht, oder nur das aus dem Saamen gepreßte
|||||||||||||||||||||||||||||||||||||||||||||||||||
Eſſig gemacht, oder nur das aus dem Saamen gepreßte

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010019.gt.txt
Oel mit Eſſig vermiſcht und an die Stirne und Schla◌ͤfe ge⸗
#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Del mit Eſſig vermiſcht und an die Stirne und Schla◌ͤfe ge⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001a.gt.txt
ſtrichen, macht guten Schlaf, welchen auch ein von Bil⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
ſtrichen, macht guten Schlaf, welchen auch ein von Bil⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001b.gt.txt
ſenkraut zubereitetes Fußbad bewirkt.
#||||||||||||||||||||||||||||||||||||
fenkraut zubereitetes Fußbad bewirkt.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001c.gt.txt
Bin~gelk~r~a~u~t, Mer~curi~u~skra~u~t, iſt entweder
|||+||||+|+|+|+||||||+#|||+|+||||+|+|||||||||||||||
Bin gelk r a u t, Mere uri u skra u t, iſt entweder

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001d.gt.txt
zahm oder wild. Das z~ahm~e Bin~gelkraut hat zweier⸗
|||||||||||||||||||||+|||+|||#|+|||||||||||||||||||-
zahm oder wild. Das z ahm e B1n gelkraut hat zweier~

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001e.gt.txt
lei Geſchlechter, Ma◌ͤnnch~en und We~ib~ch~en. Das
|||||||||||||||||||##|#||+|||||||||+#|+||+|||||||
lei Geſchlechter, Männ ch en und Wel b ch en. Das

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/01001f.gt.txt
Weibchen wa◌ͤchst anderthalb Schuh hoch, bringt einen
||||||||||||||||||||||||||||||||||||||||||||||||||||
Weibchen wa◌ͤchst anderthalb Schuh hoch, bringt einen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010020.gt.txt
eckigen, glatten, mit vielen Zweigen beſetzten Stengel,
|||||||||||||||||||||||||||||||||||||||#|||||||||||||||
eckigen, glatten, mit vielen Zweigen befetzten Stengel,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010021.gt.txt
aus deſſen Knoten die Bla◌ͤtter entſpringen, von gelbgru◌ͤner
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
aus deſſen Knoten die Bla◌ͤtter entſpringen, von gelbgru◌ͤner

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0019/010022.gt.txt
Farbe, am Umkreiſe zerkerbt und von widerwa◌ͤrtigem Ge⸗
||||||||||||||||||||||||||||||||||||||||||||||||||||||
Farbe, am Umkreiſe zerkerbt und von widerwa◌ͤrtigem Ge⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010001.gt.txt
ſitzen und ohne Saamen abfallen, hat ſchwache mit vielen
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
ſitzen und ohne Saamen abfallen, hat ſchwache mit vielen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010002.gt.txt
Faſern bewachſene Wurzeln. Das Ma◌ͤn~nchen hat etwas
|||||||||||||||||||||||||||||||||#|+#||||||||||||||
Faſern bewachſene Wurzeln. Das Maän uchen hat etwas

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010003.gt.txt
ſchwa◌ͤrzere Bla◌ͤtter, und bei den Knoten ſeinen runden und
|||||||||||||||||||||||||||||||||||||||||||||||||||||#||||
ſchwa◌ͤrzere Bla◌ͤtter, und bei den Knoten ſeinen rundeu und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010004.gt.txt
rauhen Saamen, je zwei und zwei Ko◌ͤrnchen nebeneinander.
|||||||||||||||#|||||#|||||||||||-#|||||||||||||||||||||
rauhen Saamen, ze zwet und zwei K~brnchen nebeneinander.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010005.gt.txt
Sie lieben einen guten Grund, und wachſen gerne in den
|||||||||||||||#|||||||||||||||||||||||||||||||||||||#
Sie lieben eineu guten Grund, und wachſen gerne in deu

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010006.gt.txt
Weinbergen, geben aber dem Weine einen unangenehmen
||||||||||||||||||||||||||||||#||||||||||||||||||||
Weinbergen, geben aber dem Weiue einen unangenehmen

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010007.gt.txt
Geſchmack, daher ſie auch daſelbſt nicht gerne gelitten wer⸗
||||||||||||||||||||||||||||||||||||||||||||#|||||||||||||||
Geſchmack, daher ſie auch daſelbſt nicht gerue gelitten wer⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010008.gt.txt
den. — Des wi~lden Bi~n~g~e~lkr~a~utes, welches auch
|||||#|||||||+|||||||+|+|+|+|||+|+||||||||||||||||||
den. - Des wi lden Bi n g e lkr a utes, welches auch

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010009.gt.txt
Hun~d~sko~hl genannt, und in Wa◌ͤldern, auf hohen Bergen,
|||+|+|||+||||||||||||||||||||||||||||||||||||||||||||||
Hun d sko hl genannt, und in Wa◌ͤldern, auf hohen Bergen,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000a.gt.txt
auch an Za◌ͤunen und Hecken gefunden wird, ſind ebenfalls,
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||
auch an Za◌ͤunen und Hecken gefunden wird, ſind ebenfalls,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000b.gt.txt
wie des zahmen, zwei Geſchlechter, M~a◌ͤn~n~ch~en und
||||||||||||||||||||||||||||||||||||+|||+|+||+||||||
wie des zahmen, zwei Geſchlechter, M a◌ͤn n ch en und

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000c.gt.txt
W~eib~chen. Sie unterſcheiden ſich nur durch den Saa⸗
|+|||+|||||||||||||||||||||||||||||||||||||||||||||||
W eib chen. Sie unterſcheiden ſich nur durch den Saa⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000d.gt.txt
men, der bei dem Ma◌ͤnnchen in 2 runden Ko◌ͤrnchen beſteht,
|||||||||||||||||||||#||||||||--||||||||-#|#|||||||||||||
men, der bei dem Ma◌ͤnuchen in ~~runden K~bruchen beſteht,

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000e.gt.txt
bei dem Weibchen wie eine kleine Traube zuſammengewach⸗
|||||||||||||||||||||||||||||||||||||||||||||||||||||||
bei dem Weibchen wie eine kleine Traube zuſammengewach⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/01000f.gt.txt
ſen iſt. Jhr Stengel iſt zart, weißlich, und wird ſelten
||||||||||||||||||||||||||||||||||||||||||||||||||||||||
ſen iſt. Jhr Stengel iſt zart, weißlich, und wird ſelten

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010010.gt.txt
u◌ͤber eine Spanne hoch; die Bla◌ͤtter ſind wie die am zah⸗
||||||||||||||||||||||||||||||-#||||||||||||||||||||||||#
u◌ͤber eine Spanne hoch; die Bl~ätter ſind wie die am zahe

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010011.gt.txt
men Bingelkraute. Der Saame iſt rund und blaulich, die
||||||||||||||||||||||||||||||||||||||||||||||||||||||
men Bingelkraute. Der Saame iſt rund und blaulich, die

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010012.gt.txt
~Wurzel aber weiß, faſerig, und kriecht in der Erde weit umher.
+||||||||||||||||||||||||||||||||||||||||||#|||||||||||||||||||
bWurzel aber weiß, faſerig, und kriecht in ver Erde weit umher.

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010013.gt.txt
Ge~b~rauch. Beide Arten und Geſchlechter haben
||+|+|||||||||||||-|||||||||||||||||||||||||||
Ge b rauch. Beide ~rten und Geſchlechter haben

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010014.gt.txt
einerlei Wirkung, indem ſie o◌ͤffnen und durch den Stuhl⸗
||||||||||||||||||||||||||||-#||||||||||||||||||||||||||
einerlei Wirkung, indem ſie ~bffnen und durch den Stuhl⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010015.gt.txt
gang die gallige und wa◌ͤſſerige Feuchtigkeit abfu◌ͤhren, da⸗
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
zang die gallige und wa◌ͤſſerige Feuchtigkeit abfu◌ͤhren, da⸗

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010016.gt.txt
bei gelinde laxiren, und bei den Kindern, wenn man es
||||||||||||||#||||||||||||||||||||||||||||||||||||||
bei gelinde lariren, und bei den Kindern, wenn man es

fraktur1/1828-DieEigenschaftenAllerHeilpflanzen-NN/0020/010017.gt.txt
ihnen im Brei (~Muß)~ eingiebt, das Reißen im Leibe verhu◌ͤten.
|||||||||||||||+||||+|||||||||||||||||||||||||||||||||||||||||
ihnen im Brei ( Muß)j eingiebt, das Reißen im Leibe verhu◌ͤten.

