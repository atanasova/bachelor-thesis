fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010001.gt.txt
Die Abtheilung GE. macht in Anſehung des
|||||||||||||||----|||||||||||||||||||||
Die Abtheilung ~~~~macht in Anſehung des

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010002.gt.txt
Standes der Staubfä◌ͤden eine Ausnahme, wo⸗
||||||||||||||||||#|||||||||||||||||||||||
Standes der Staubfa◌ͤden eine Ausnahme, wo⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010003.gt.txt
von nachher. Sind ohne Ausnahme Zwitter.
||||||||||||||||||||||||||||||||||||||||
von nachher. Sind ohne Ausnahme Zwitter.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010004.gt.txt
GA. Lauter Kra◌ͤuter. Jhre Bla◌ͤtter ſtehen von einer
--#||||||||||||||||||||||||||||||||||||||||||||||||
~~k Lauter Kra◌ͤuter. Jhre Bla◌ͤtter ſtehen von einer

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010005.gt.txt
Seite zur andern am Stamm, ohne Stiel,
||||||||||||||||||||||||||||||||||||||
Seite zur andern am Stamm, ohne Stiel,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010006.gt.txt
und ſind meiſtentheils mit ſteifen Haaren und
|||||||||||||||||||||||||||||||||||||||||||||
und ſind meiſtentheils mit ſteifen Haaren und

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010007.gt.txt
rauhen Puncten beſetzt, und rauh anzufu◌ͤhlen.
||||||||||#|||||||||-||||||||||||||||||||||||
rauhen Puneten beſet~t, und rauh anzufu◌ͤhlen.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010008.gt.txt
Die Blu◌ͤthen ſtehen in einer einſeitigen Aeh⸗
||||||#||||||||||||||||||||||||||||||||||||||
Die Blü◌ͤthen ſtehen in einer einſeitigen Aeh⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010009.gt.txt
re, die anfa◌ͤnglich ſpiralfo◌ͤrmig von der Spitze
||||||||||||||||||||||||||||||||||||||||||||||||
re, die anfa◌ͤnglich ſpiralfo◌ͤrmig von der Spitze

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000a.gt.txt
ab nach unten zu eingerollet iſt, und wa◌ͤhrenden
||||||||||||||||||||||||||||||||||||||||||||||||
ab nach unten zu eingerollet iſt, und wa◌ͤhrenden

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000b.gt.txt
Blu◌ͤhens ſich aufrollet und gerade ſtrecket.
||||||||||||||||||||||||||||||||||||||||||||
Blu◌ͤhens ſich aufrollet und gerade ſtrecket.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000c.gt.txt
Blume und Blumendecke ſind fu◌ͤnftheiligt,
|||||||||||||||||||||||||||||||||||||||||
Blume und Blumendecke ſind fu◌ͤnftheiligt,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000d.gt.txt
wenige aus~genommen, regula◌ͤr. An vielen fin⸗
||||||||||+||||||||||||||||#|||-|||||||||||||
wenige aus◌ͤgenommen, regulaär. ~n vielen fin⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000e.gt.txt
den ſich im Schlunde der Blume zwiſchen der
|||||||||||||||||||||||||||||||||||||||||||
den ſich im Schlunde der Blume zwiſchen der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01000f.gt.txt
Ro◌ͤhre und Mu◌ͤndung gewiſſe Anſa◌ͤtze. Der
||#||||||||||-|||||||||||||||||||||||||||
Roshre und Mu~ndung gewiſſe Anſa◌ͤtze. Der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010010.gt.txt
Staubfa◌ͤden ſind fu◌ͤnf. Ein griffelfo◌ͤrmiger
|||||||||||||||||||-|||||||||||||||||#||||||
Staubfa◌ͤden ſind fu~nf. Ein griffelfohrmiger

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010011.gt.txt
Staubweg mit einfachem Stigma, um deſſen
||||||||||||||||||||||||||||||||||||||||
Staubweg mit einfachem Stigma, um deſſen

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010012.gt.txt
Baſis vier Saamko◌ͤrner ſtehen, und mehr
|||||||||||||||||-|||||||||||||||||||||
Baſis vier Saamko~rner ſtehen, und mehr

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010013.gt.txt
oder weniger mit dem Griffel zuſammenha◌ͤngen.
|||||||||||||||||||||||||||||||||||||||||||||
oder weniger mit dem Griffel zuſammenha◌ͤngen.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010014.gt.txt
Jn einigen ſcha◌ͤlt ſich von dem Saam~korn eine
||||||||||||||||||||||||||||||||||||+|||||||||
Jn einigen ſcha◌ͤlt ſich von dem Saam korn eine

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010015.gt.txt
Haut ab, ſo daß man es fu◌ͤglich als eine Capſel
|||||||||||||||||||||||||-|||||||||||||||||||||
Haut ab, ſo daß man es fu~glich als eine Capſel

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010016.gt.txt
anſehen kann. Ueberhaupt muß man ſich die
|||||||||||||||||||||||||||||||||||||||||
anſehen kann. Ueberhaupt muß man ſich die

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010017.gt.txt
Frucht dieſer Blumen als eine in Fa◌ͤcher abge⸗
||||||||||||||||||||||||||||||||||||||||||||||
Frucht dieſer Blumen als eine in Fa◌ͤcher abge⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010018.gt.txt
theilte von ihrer Schale entkleidete Capſel vor⸗
||||||||||||||||||||||||||||||||||||||||||||||||
theilte von ihrer Schale entkleidete Capſel vor⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/010019.gt.txt
ſtellen, in welcher der Griffel das Sa◌ͤulgen iſt,
|||||||||||||||||||||||||||||||||||||||||||||||||
ſtellen, in welcher der Griffel das Sa◌ͤulgen iſt,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01001a.gt.txt
an dem die Saamen ha◌ͤngen, oder als vier im
|||||||||||||||||||||||||||||||||||||||||||
an dem die Saamen ha◌ͤngen, oder als vier im

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01001b.gt.txt
Kreyß um eine Sa◌ͤule herumſtehende Capſeln,
|||||||||||||||||||||||||||||||||||||||||||
Kreyß um eine Sa◌ͤule herumſtehende Capſeln,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0033/01001c.gt.txt
ſo wie bey HC. gezeigt werden wird. Jn den
|||||||||||###||||||||||||||||||||||||||||
ſo wie bey k C gezeigt werden wird. Jn den

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010001.gt.txt
meiſten dient die Blumendecke der reiffenden
||||||||||||||||||||||||||||||||||||||||||||
meiſten dient die Blumendecke der reiffenden

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010002.gt.txt
Frucht zur Bedeckung.
|||||||||||||||||||||
Frucht zur Bedeckung.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010003.gt.txt
§. 197.
-||-##|
~. ~z/.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010004.gt.txt
GB. Haben einen viereckigten Stamm, gepaarte
-#||||||||||||||||||||||||||||||||||||||||||
~6. Haben einen viereckigten Stamm, gepaarte

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010005.gt.txt
Aeſte ins Kreutz, gegen einander u◌ͤberſtehende
-||||||||||||||-||#|||||||||||||||-|||||||||||
~eſte ins Kreut~, zegen einander u~berſtehende

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010006.gt.txt
Bla◌ͤtter ohne Stiele, und Fructificationen aus
|||||||||||||||||||||||||||||#||||||||||||||||
Bla◌ͤtter ohne Stiele, und Fruetificationen aus

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010007.gt.txt
den Win~keln der Bla◌ͤtter, die um den Stamm
|||||||+|||||||||||||||||||||||||||||||||||
den Win keln der Bla◌ͤtter, die um den Stamm

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010008.gt.txt
in einem Quirl oder Kranz anliegen, zuweilen
||||||||||||||||||||||||||||||||||||||||||||
in einem Quirl oder Kranz anliegen, zuweilen

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010009.gt.txt
auch auf einem gemeinſchaftlichen Hauptſtengel
||||||||||||||||||||||||||||||||||||||||||||||
auch auf einem gemeinſchaftlichen Hauptſtengel

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000a.gt.txt
ſich empor heben. Der Stamm iſt in den mei⸗
|||||||||||||||||||||||||||||||||||||||||||
ſich empor heben. Der Stamm iſt in den mei⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000b.gt.txt
ſten gerade, an ho◌ͤlzernen Faſern ziemlich reich,
|||||||||||||||||||||||||||||||||||||||||||||||||
ſten gerade, an ho◌ͤlzernen Faſern ziemlich reich,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000c.gt.txt
wird oft zu einer Staude, doch iſt unter unſern
|||||||||||||||||||||||||||||||||||||||||||||||
wird oft zu einer Staude, doch iſt unter unſern

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000d.gt.txt
einheimiſchen kein Baum. Die Bla◌ͤtter haben
|||||||||||||||||||||||||||||||||||||||||||
einheimiſchen kein Baum. Die Bla◌ͤtter haben

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000e.gt.txt
ein ſehr ſtarkes Gea◌ͤder, ſind oft runzlicht, oft
|||||||||||||||||||||||||||||||||||||||||||||||||
ein ſehr ſtarkes Gea◌ͤder, ſind oft runzlicht, oft

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01000f.gt.txt
haarigt, oft mit tiefen Puncten beſtreut, mei⸗
|||||||||||||||||||||||||||#||||||||||||||||||
haarigt, oft mit tiefen Puneten beſtreut, mei⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010010.gt.txt
ſtens einfach, und nur ſelten zerſtu◌ͤckt. Die
||||||||||||||||||||||||||||||||||||-||||||||
ſtens einfach, und nur ſelten zerſtu~ckt. Die

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010011.gt.txt
Blumenkra◌ͤnze ſind an vielen mit Blu◌ͤthenbla◌ͤt⸗
||||||||||||||||||||||||||||||||||||-||||||||||
Blumenkra◌ͤnze ſind an vielen mit Blu~thenbla◌ͤt⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010012.gt.txt
tern belegt.
||||||||||||
tern belegt.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010013.gt.txt
Blume und Blumendecke ſind irregula◌ͤr.
||||||||||||||||||||||||||||||||||||||
Blume und Blumendecke ſind irregula◌ͤr.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010014.gt.txt
Letztere hat immer eine tiefe Ro◌ͤhre oder Bauch,
||||||||||||||||||||||||||||||||||||||||||||||||
Letztere hat immer eine tiefe Ro◌ͤhre oder Bauch,

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010015.gt.txt
in welchem die Saamen reifen, die Mu◌ͤndung
||||||||||||||||||||||||||||||||||||-|||||
in welchem die Saamen reifen, die Mu~ndung

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010016.gt.txt
aber iſt in einigen regula◌ͤr mit fu◌ͤnf Lappen oder
|||||||||||||||||||||||||||||||||||-||||||||||||||
aber iſt in einigen regula◌ͤr mit fu~nf Lappen oder

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010017.gt.txt
Za◌ͤhnen, in den meiſten irregula◌ͤr mit einer lip⸗
|||||||||||||||||||||||||||||||||||||||||||||||||
Za◌ͤhnen, in den meiſten irregula◌ͤr mit einer lip⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010018.gt.txt
penfo◌ͤrmigen Eintheilung, meiſtens mit drey
|||||||||||||||||||||||||||||||||||||||||||
penfo◌ͤrmigen Eintheilung, meiſtens mit drey

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/010019.gt.txt
Za◌ͤhnen in der obern, mit zweyen in der untern
||||||||||||||||||||||||||||||||||||||||||||||
Za◌ͤhnen in der obern, mit zweyen in der untern

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0034/01001a.gt.txt
Lippe.
#|||||
Eippe.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010001.gt.txt
Die Blume hat eine meiſtens u◌ͤber den
|||||||||||||||||||||||||||||-|||||||
Die Blume hat eine meiſtens u~ber den

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010002.gt.txt
Bauch der Decke hervorragende Ro◌ͤhre, und
|||||||||||||||||||||||||||||||||||||||||
Bauch der Decke hervorragende Ro◌ͤhre, und

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010003.gt.txt
eine lippenfo◌ͤrmige Mu◌ͤndung mit fu◌ͤnf Haupt⸗
||||||||||||||||||||||-|||||||||||#||||||||||
eine lippenfo◌ͤrmige Mu~ndung mit fü◌ͤnf Haupt⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010004.gt.txt
einſchnitten, da zwey aufwa◌ͤrts gerichtete Lap⸗
|||||||||||||||||||||||||||#|||||||||||||||||||
einſchnitten, da zwey aufwaärts gerichtete Lap⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010005.gt.txt
pen zur obern, drey in horizonteller Lage zur
|||||||||||||||||||||||||||||||||||||||||||||
pen zur obern, drey in horizonteller Lage zur

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010006.gt.txt
untern Lippe geho◌ͤren, unter welchen der m~ittel⸗
||||||||||||||||||||||||||||||||||||||||||+||||||
untern Lippe geho◌ͤren, unter welchen der m ittel⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010007.gt.txt
ſte gewo◌ͤhnlich u◌ͤber die Seitenlappen herfu◌ͤrra⸗
|||||||-||||||||-#||||||||||||||||||||||||||-||||
ſte gew~◌ͤhnlich ~über die Seitenlappen herfu~rra⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010008.gt.txt
get. Die zwey Lappen der obern Lippe ſind
|||||||||||||||||||||||||||||||||||||||||
get. Die zwey Lappen der obern Lippe ſind

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010009.gt.txt
oft zuſammengewachſen, und machen ein helm⸗
|||||||||||||||||||||||||||||||||||||||||||
oft zuſammengewachſen, und machen ein helm⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000a.gt.txt
fo◌ͤrmiges Gewo◌ͤlbe u◌ͤber die Staubfa◌ͤden. Zu⸗
||-||||||||||||||||-#||||||||||||||||||||||||
fo~rmiges Gewo◌ͤlbe ~über die Staubfa◌ͤden. Zu⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000b.gt.txt
weilen ſcheint die obere Lippe zu verſchwin⸗
|||||||||||||||||||||||||#||||||||||||||||||
weilen ſcheint die obere Eippe zu verſchwin⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000c.gt.txt
den, oder iſt flach ausgebogen, und die ganze
|||||||||||||||||||||||||||||||||||||||||||||
den, oder iſt flach ausgebogen, und die ganze

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000d.gt.txt
Mu◌ͤndung erha◌ͤlt ein regula◌ͤres Anſehen. Der
||-|||||||||||||||||||||||||||||||||||||||||
Mu~ndung erha◌ͤlt ein regula◌ͤres Anſehen. Der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000e.gt.txt
Ru◌ͤcken der Blume iſt einwa◌ͤrts, die Oefnung
||-||||||||||||||||||||||||||||||||||#||||||
Ru~cken der Blume iſt einwa◌ͤrts, die Defnung

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01000f.gt.txt
zwiſchen den Lippen, und die untere Lippe aus⸗
||||||||||||||||||||||||||||||||||||||||||||||
zwiſchen den Lippen, und die untere Lippe aus⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010010.gt.txt
wa◌ͤrts gekehret, doch findet ſich in einigen aus⸗
|||||||||||||||||||||||||||||||||||||||||||||||||
wa◌ͤrts gekehret, doch findet ſich in einigen aus⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010011.gt.txt
la◌ͤndiſchen Arten eine umgekehrte Lage.
|||||||||||||||||||||||||||||||||||||||
la◌ͤndiſchen Arten eine umgekehrte Lage.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010012.gt.txt
Der Staubfa◌ͤden ſind vier, in zwey Paa⸗
|||||||||||||||||||||||||||||||||||||||
Der Staubfa◌ͤden ſind vier, in zwey Paa⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010013.gt.txt
ren, ein la◌ͤngeres und dem Stande nach ho◌ͤhe⸗
|||||||||||||||||||||||||||||||||||||||||||||
ren, ein la◌ͤngeres und dem Stande nach ho◌ͤhe⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010014.gt.txt
res, ein ku◌ͤrzeres und der Lage nach niedrigeres
|||#|-|||||-||||||||||||||||||||||||||||||||||||
resz ~in ku~rzeres und der Lage nach niedrigeres

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010015.gt.txt
Paar, aus der Ro◌ͤhre der Blume auf der
#|||||||||||||||-|||||||||||||||||||||
Daar, aus der Ro~hre der Blume auf der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010016.gt.txt
Ru◌ͤckenſeite und unter der obern Lippe. Jhre
||||||||||||||||||||||||||||||||||||||||||||
Ru◌ͤckenſeite und unter der obern Lippe. Jhre

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010017.gt.txt
Fa◌ͤden ziehen ſich langs der Ro◌ͤhre als merkliche
|||||||||||||||||||||#|||||||||||||||||||||||||||
Fa◌ͤden ziehen ſich laugs der Ro◌ͤhre als merkliche

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010018.gt.txt
Streifen herunter. Jn einigen Gattungen fin⸗
||||||||||||||||||||||||||||||||||||||||||||
Streifen herunter. Jn einigen Gattungen fin⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/010019.gt.txt
det ſich nur ein Paar Staubfa◌ͤden.
||||||||||||||||||||||||||||||||||
det ſich nur ein Paar Staubfa◌ͤden.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01001a.gt.txt
Der Staubweg ko◌ͤmmt aus der Mitte der
||||||||||||||--|||||||||||||||||||||
Der Staubweg k~~mmt aus der Mitte der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01001b.gt.txt
vier Saamko◌ͤrner, beugt ſich nach dem Ru◌ͤcken
|||||||||||#|||||||||||||||||||||||||||||||||
vier Saamkoorner, beugt ſich nach dem Ru◌ͤcken

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0035/01001c.gt.txt
der Blume zwiſchen den zwey Paaren der
||||||||||||||||||||||||||||||||||||||
der Blume zwiſchen den zwey Paaren der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010001.gt.txt
Staubfa◌ͤden und ſpaltet ſich gabelfo◌ͤrmig in zwey
|||||||||||||||||||||||||||||||||||||||||||||||||
Staubfa◌ͤden und ſpaltet ſich gabelfo◌ͤrmig in zwey

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010002.gt.txt
ſpitzige etwas ungleiche Stigmate, deren eines
||||-|||||||||||||||||||||||||||||||||||||||||
ſpit~ige etwas ungleiche Stigmate, deren eines

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010003.gt.txt
nach dem Ru◌ͤcken der Blume, das ku◌ͤrzere aus~⸗
||||||||||||||||||||||||||||||||||||||||||||+|
nach dem Ru◌ͤcken der Blume, das ku◌ͤrzere aus◌ͤ⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010004.gt.txt
wa◌ͤrts gebogen iſt.
|||||||||||||||||||
wa◌ͤrts gebogen iſt.

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010005.gt.txt
Die Frucht beſteht aus vier um den Staub⸗
|||||||||||||||||||||||||||||||||||||||||
Die Frucht beſteht aus vier um den Staub⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010006.gt.txt
weg anliegenden Ko◌ͤrnern, und man ſtellet ſich
||||||||||||||||||||||||||||||||||||||||||||||
weg anliegenden Ko◌ͤrnern, und man ſtellet ſich

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010007.gt.txt
dieſe Frucht am richtigſten als eine von ihrer
||||||||||||||||||||||||||||||||||||||||||||||
dieſe Frucht am richtigſten als eine von ihrer

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010008.gt.txt
Schale entkleidete Capſel vor, worin der
||||||||||||||||||||||||||||||||||||||||
Schale entkleidete Capſel vor, worin der

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/010009.gt.txt
Staubweg mit ſeiner Baſis das Sa◌ͤulgen aus⸗
|||||||||||||||||||||||||||||||||||||||||||
Staubweg mit ſeiner Baſis das Sa◌ͤulgen aus⸗

fraktur1/1764-EinleitungZuDerKräuterkenntniß-Oeder/0036/01000a.gt.txt
macht.
||||||
macht.

