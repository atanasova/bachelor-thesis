fraktur2/1487-GartDerGesunthait/0010/010001.gt.txt
Eppich ˖
-|||||-#
~ppich~.

fraktur2/1487-GartDerGesunthait/0010/010002.gt.txt
Capitulum ˖
#||||||||-#
Kapitulum~-

fraktur2/1487-GartDerGesunthait/0010/010003.gt.txt
vi ˖
||-#
vi~.

fraktur2/1487-GartDerGesunthait/0010/010004.gt.txt
Pium latine ˖ Grece ſeluiũ ˖
|||||||||||||||||||||||||-##
Pium latine ˖ Grece ſelui~ſ.

fraktur2/1487-GartDerGesunthait/0010/010005.gt.txt
¶ Der maiſter Platearius
||||||||||||||||||||||||
¶ Der maiſter Platearius

fraktur2/1487-GartDerGesunthait/0010/010006.gt.txt
ſpricht das epich ſei haiſz vñ truck
||||||||||||||||||||||||||||||||||||
ſpricht das epich ſei haiſz vñ truck

fraktur2/1487-GartDerGesunthait/0010/010007.gt.txt
en bei dem driten grad ˖ ¶ Der wir
||||||||||||||||||||||||||||||||||
en bei dem driten grad ˖ ¶ Der wir

fraktur2/1487-GartDerGesunthait/0010/010008.gt.txt
dig maiſter Auicenna ſpricht ˖ das
-|||||||||||||||||||||||||||||||||
~ig maiſter Auicenna ſpricht ˖ das

fraktur2/1487-GartDerGesunthait/0010/010009.gt.txt
eppich ſei warm in dẽ erſten grad
-|||||||||||||||||||||||#||||||||
~ppich ſei warm in dẽ er ten grad

fraktur2/1487-GartDerGesunthait/0010/01000a.gt.txt
vnd tru~cken in dem zwaiten grad
|||||||+||||||||||||||||||||||||
vnd tru cken in dem zwaiten grad

fraktur2/1487-GartDerGesunthait/0010/01000b.gt.txt
Vnd der ſame von eppich mer ge
-|||||||||||||||||||||||||||||
~nd der ſame von eppich mer ge

fraktur2/1487-GartDerGesunthait/0010/01000c.gt.txt
nützt werde in der ertznei dañ das
||||||||||||||||||||||||||||||||||
nützt werde in der ertznei dañ das

fraktur2/1487-GartDerGesunthait/0010/01000d.gt.txt
kraut oder die wurtz ˖ ¶ Diſz kraut
|||||||||||||||||||#|--||||||||||||
kraut oder die wurty ~~¶ Diſz kraut

fraktur2/1487-GartDerGesunthait/0010/01000e.gt.txt
gleichet den kerbeln vñ hat weiſſe
||||||||||||||||||||||||||||||||||
gleichet den kerbeln vñ hat weiſſe

fraktur2/1487-GartDerGesunthait/0010/01000f.gt.txt
plümlin ˖ ¶ Merck wenn man ſch⸗
|||||||||||||||||||||||||||||||
plümlin ˖ ¶ Merck wenn man ſch⸗

fraktur2/1487-GartDerGesunthait/0010/010010.gt.txt
reibt in den recepten apiũ ſo mai⸗
#||||||||||||||||||||||||#||||||||
teibt in den recepten apiñ ſo mai⸗

fraktur2/1487-GartDerGesunthait/0010/010011.gt.txt
nent die ertzt den ſame vñ nit das
||||||#|||||||||||||||||||||||||||
nent dte ertzt den ſame vñ nit das

fraktur2/1487-GartDerGesunthait/0010/010012.gt.txt
kraut noch die wurtzl ˖ ¶ Eppich
|||||||||||||||||||||---||||||||
kraut noch die wurtzl~~~¶ Eppich

fraktur2/1487-GartDerGesunthait/0010/010013.gt.txt
ſamen gepuluert vñ eingenomen
#||||||||||||||||||||||||||||
lamen gepuluert vñ eingenomen

fraktur2/1487-GartDerGesunthait/0010/010014.gt.txt
mit rettichwaſſer macht wol har
|||||||-#||||#|||||||||||||||||
mit ret~achwafſer macht wol har

fraktur2/1487-GartDerGesunthait/0010/010015.gt.txt
men ˖ vnd zerbricht den ſtain in der
||||--||||||||||||||||||||||||||||||
men ~~vnd zerbricht den ſtain in der

fraktur2/1487-GartDerGesunthait/0010/010016.gt.txt
plaſen vñ auch in den lendẽ ˖ ¶ Aui
|||||||##||||||||||||||||--|--|||||
plaſen rſ auch in den len~~ ~~¶ Aui

fraktur2/1487-GartDerGesunthait/0010/010017.gt.txt
cenna ſpricht ˖ das in der wurtzelv
#||||||||||||-||||||||||||||||||||-
tenna ſpricht~˖ das in der wurtzel~

fraktur2/1487-GartDerGesunthait/0010/010018.gt.txt
mer kr~afft ſei dann in dem ſamen ˖
||||||+|||||||#||||||#|||||||||||-#
mer kr afft ſej dann mn dem ſamen~-

fraktur2/1487-GartDerGesunthait/0010/010019.gt.txt
vnd der ſamen mer krafft hat dañ
||||||||||||||||||||||||||||||||
vnd der ſamen mer krafft hat dañ

fraktur2/1487-GartDerGesunthait/0010/01001a.gt.txt
das kraut ˖ ¶ Der maiſter Yſaac ˖
||||||||||||||||||||||||||#|||||#
das kraut ˖ ¶ Der maiſter Wſaac -

fraktur2/1487-GartDerGesunthait/0010/01001b.gt.txt
in ſeinem bůch ˖ ge~nannt de dietis
||||||||||||||-||||+|||||||||||||||
in ſeinem bůch~˖ ge nannt de dietis

fraktur2/1487-GartDerGesunthait/0010/01001c.gt.txt
particularibus ˖ in dem capitel api
||||-|||||||||||||||||||||||||||||#
part~cularibus ˖ in dem capitel apt

fraktur2/1487-GartDerGesunthait/0010/01001d.gt.txt
um ſpricht ˖ das eppich gemü~ſcht
||||||||||||||||||||||||||||+||||
um ſpricht ˖ das eppich gemüiſcht

fraktur2/1487-GartDerGesunthait/0010/01001e.gt.txt
mit wein ſüſz gemacht mit honig
|||||||||||-#||||||||||||||||||
mit wein ſü~f gemacht mit honig

fraktur2/1487-GartDerGesunthait/0010/01001f.gt.txt
genannt mellecrat ˖ macht den mẽ
||||||||||||||||||||||||||||||||
genannt mellecrat ˖ macht den mẽ

fraktur2/1487-GartDerGesunthait/0010/010020.gt.txt
ſchen wol harmen ˖ ¶ Vñ allſo ge
|||||||||||||||||||||-||||||||||
ſchen wol harmen ˖ ¶ ~ñ allſo ge

fraktur2/1487-GartDerGesunthait/0010/010021.gt.txt
nützet bringt auch die kranckheit
|||||||||||||||||||||||||||||||||
nützet bringt auch die kranckheit

fraktur2/1487-GartDerGesunthait/0010/010022.gt.txt
der fra~wen genannt menſtruum ˖
|||||||+|||||||||||||||||||||-#
der fra wen genannt menſtruum~⸗

fraktur2/1487-GartDerGesunthait/0010/010023.gt.txt
¶ Epich allſo genü~tzt macht wol
|||||||||||-||||||+|||||||||||||
¶ Epich all~o genüitzt macht wol

fraktur2/1487-GartDerGesunthait/0010/010024.gt.txt
dewen ˖ ¶ Plinius ˖ Epichwurtzel
|||||||||||||||#|||-||#|||||||||
dewen ˖ ¶ Plinins ˖~Eprchwurtzel

fraktur2/1487-GartDerGesunthait/0010/010025.gt.txt
geſoten mit wein vñ den getrunck
||||||||||||||||||||||||||||||||
geſoten mit wein vñ den getrunck

fraktur2/1487-GartDerGesunthait/0010/010026.gt.txt
en treibt aus den ſtain in der plaſ⸗
||||||||||||||||||||||||||||||||||||
en treibt aus den ſtain in der plaſ⸗

fraktur2/1487-GartDerGesunthait/0010/010027.gt.txt
en vnd au~ch in den lenden ˖ ¶ Dia⸗
|||||||||+|||||||||||||||||#|||||||
en vnd au ch in den lenden - ¶ Dia⸗

fraktur2/1487-GartDerGesunthait/0010/010028.gt.txt
ſc~orides in dem capitel apiũ ſpricht
||+|||||||||||||||||||||||||#||||||||
ſc orides in dem capitel apiñ ſpricht

fraktur2/1487-GartDerGesunthait/0010/010029.gt.txt
das der ſamen von epich vaſt gůt
||||||||||||||||||||||||||#|||||
das der ſamen von epich vaft gůt

fraktur2/1487-GartDerGesunthait/0010/01002a.gt.txt
ſei dem der nit hermen mag ˖ ¶ Jtẽ
|||||||||||||||||||||||||||--|||||
ſei dem der nit hermen mag ~~¶ Jtẽ

fraktur2/1487-GartDerGesunthait/0010/01002b.gt.txt
die wur~czel von eppich geſotten
|||||||+#|||||||||||||||||||||||
die wurt zel von eppich geſotten

fraktur2/1487-GartDerGesunthait/0010/01002c.gt.txt
in wein vnd den getruncken treibt
|||||||||||||||||||||||||||||||||
in wein vnd den getruncken treibt

fraktur2/1487-GartDerGesunthait/0010/01002d.gt.txt
aus dem menſchen vergifft ˖ Vnd
||||||||||||||||||||||||||||-||
aus dem menſchen vergifft ˖ ~nd

fraktur2/1487-GartDerGesunthait/0010/01002e.gt.txt
allſo genützet benimet das brech
||||||||||||||||||||||||||||||||
allſo genützet benimet das brech

fraktur2/1487-GartDerGesunthait/0010/01002f.gt.txt
en ˖ genannt vomitum ˖ vnd o◌ͤffnet
|||--||||||||||||||||#|||||--|||||
en ~~genannt vomitum - vnd ~~ffnet

fraktur2/1487-GartDerGesunthait/0010/010030.gt.txt
den zerſchwollen magen ˖ ¶ Der
||||||||||||||||||||||||||||||
den zerſchwollen magen ˖ ¶ Der

fraktur2/1487-GartDerGesunthait/0010/010031.gt.txt
maiſter Galienus in dem bůch ge
||#||||||||-|||||||||||||||||||
matſter Gal~enus in dem bůch ge

fraktur2/1487-GartDerGesunthait/0010/010032.gt.txt
nannt de agricultura ſpricht ˖ das
||||||||||||||||||||||||||||||||||
nannt de agricultura ſpricht ˖ das

fraktur2/1487-GartDerGesunthait/0010/010033.gt.txt
eppichſamen bring luſt den man⸗
||||||#||||||||||||||||||||||||
eppich amen bring luſt den man⸗

fraktur2/1487-GartDerGesunthait/0010/010034.gt.txt
nen vnd auch den ~frawen ˖ vñ der
|||||||||||||||||+#|||||||||###|#
nen vnd auch den t rawen ˖ vnr et

fraktur2/1487-GartDerGesunthait/0010/010035.gt.txt
vrſachen halb iſt es ver~botten ze⸗
||#|||||||||||||||||||||+||||||||||
vrlachen halb iſt es ver botten ze⸗

fraktur2/1487-GartDerGesunthait/0010/010036.gt.txt
nützen den ammen die kinder ſeü⸗
|#||||||||||||||||||||||||||||||
nutzen den ammen die kinder ſeü⸗

fraktur2/1487-GartDerGesunthait/0010/010037.gt.txt
gent ˖ wañ von groſſer begirde der
|||||--|||||||||||||||||||||||||||
gent ~~wañ von groſſer begirde der

fraktur2/1487-GartDerGesunthait/0010/010038.gt.txt
eppichſamen bringt zů vnkeü~ſch
|||||||||||||||||#|||||||||+|||
eppichſamen bringl zů vnkeü◌ͤſch

fraktur2/1487-GartDerGesunthait/0010/010039.gt.txt
heit benimt er den am~en die milch
|||||||||||||||||||||+||||||||||||
heit benimt er den am en die milch

fraktur2/1487-GartDerGesunthait/0010/01003a.gt.txt
vñ vallent die kinder darnach in
||||||||||||||||#|||||||||||||||
vñ vallent die kmnder darnach in

fraktur2/1487-GartDerGesunthait/0010/01003b.gt.txt
groſſe kranckheit ˖ ¶ Eppich ſam⸗
|||||||||||||||||||||||||||||||||
groſſe kranckheit ˖ ¶ Eppich ſam⸗

fraktur2/1487-GartDerGesunthait/0010/01003c.gt.txt
en genützt macht ain wolriechen
|||||||||||||||||||||||||||||||
en genützt macht ain wolriechen

fraktur2/1487-GartDerGesunthait/0011/010001.gt.txt
den munt ˖ darum◌̃ wel~cher mit für
||||||--|-##||#|#||||+#|||||||||-#
den mu~~ ~aBarmml wele her mit f~e

fraktur2/1487-GartDerGesunthait/0011/010002.gt.txt
ſten oder herren reden wolt d◌̉ mag
|||||||||||||||||||||||||||||-||||
ſten oder herren reden wolt d~ mag

fraktur2/1487-GartDerGesunthait/0011/010003.gt.txt
vorhin eppich brauchẽ in der koſt
|||||||||||||||||##||||||||||||||
vorhin eppich branehẽ in der koſt

fraktur2/1487-GartDerGesunthait/0011/010004.gt.txt
¶ Wer von ſucht ſein farb verlo⸗
||||||||||||||||||||||||||||||||
¶ Wer von ſucht ſein farb verlo⸗

fraktur2/1487-GartDerGesunthait/0011/010005.gt.txt
ren hette ˖ der eſz eppich ſamen teg⸗
|||||||||||||||||||||||||||||||||||||
ren hette ˖ der eſz eppich ſamen teg⸗

fraktur2/1487-GartDerGesunthait/0011/010006.gt.txt
lich in der koſt ˖ ſie wirt ym wider⸗
||||||||||||||||-|-||||||||||||||||||
lich in der koſt~˖~ſie wirt ym wider⸗

fraktur2/1487-GartDerGesunthait/0011/010007.gt.txt
komen ˖ ¶ Epich ſamen mit fench
|||||||||||||||||||||||||||||||
komen ˖ ¶ Epich ſamen mit fench

fraktur2/1487-GartDerGesunthait/0011/010008.gt.txt
elſafft vnd allſo genützt hilfft der
||||||||||||||||||||||||||||||||||||
elſafft vnd allſo genützt hilfft der

fraktur2/1487-GartDerGesunthait/0011/010009.gt.txt
geſchwollen milch in den brüſten ˖
||||||||||||||||||||||||||||||||-#
geſchwollen milch in den brüſten~⸗

fraktur2/1487-GartDerGesunthait/0011/01000a.gt.txt
allſo das ſie darnach nit ſchwern
|||||||||||||||||||||||||||||||||
allſo das ſie darnach nit ſchwern

fraktur2/1487-GartDerGesunthait/0011/01000b.gt.txt
¶ Diſz hilfft auch d◌̉ ſiechen lebern
||||||||#|||||||||||--||||||||||||||
¶ Diſz hulfft auch d~~ſiechen lebern

fraktur2/1487-GartDerGesunthait/0011/01000c.gt.txt
vñ miltz ˖ ¶ Des gleichen iſt epich
||||||||-||||||||||||||||||||||||||
vñ miltz~˖ ¶ Des gleichen iſt epich

fraktur2/1487-GartDerGesunthait/0011/01000d.gt.txt
mit peterſilgwurtzel mit wein ge⸗
|||||||||##||||||||||||||||||||||
mit peterjulgwurtzel mit wein ge⸗

fraktur2/1487-GartDerGesunthait/0011/01000e.gt.txt
ſoten den waſſerſüchtigen gůt die
|||||||||||||||||||||||||||||||||
ſoten den waſſerſüchtigen gůt die

fraktur2/1487-GartDerGesunthait/0011/01000f.gt.txt
von kalter materi komet ˖ ¶ Epich
|||||||||||||||||||||||||||||||||
von kalter materi komet ˖ ¶ Epich

fraktur2/1487-GartDerGesunthait/0011/010010.gt.txt
ſafft mit dem weiſſen ains ays zer
||||||||||||||||||||||||||||||||||
ſafft mit dem weiſſen ains ays zer

fraktur2/1487-GartDerGesunthait/0011/010011.gt.txt
ſ~chlagen ˖ vnd mit werck allſo ain
|+|||||||||||||||||||||||||||||||||
ſ chlagen ˖ vnd mit werck allſo ain

fraktur2/1487-GartDerGesunthait/0011/010012.gt.txt
pflaſter auff die wunden geleget ˖
||||||||||||||||||||||||||||||||-#
pflaſter auff die wunden geleget~⸗

fraktur2/1487-GartDerGesunthait/0011/010013.gt.txt
ſeüberet ſie ˖ ¶ Galienüs ſpricht ˖
||#||||#||||||||||||||||||||||||||#
ſeuberer ſie ˖ ¶ Galienüs ſpricht -

fraktur2/1487-GartDerGesunthait/0011/010014.gt.txt
W~elliche frawen kinder tragent ˖
|+|||||||||||||||||||||||||||||-#
WWelliche frawen kinder tragent~⸗

fraktur2/1487-GartDerGesunthait/0011/010015.gt.txt
die ſollent eppich ſammen meiden
||||||||||||||||||||||||||||||||
die ſollent eppich ſammen meiden

fraktur2/1487-GartDerGesunthait/0011/010016.gt.txt
wann an des kindes leibe werdent
||||||||||||||||||||||||||||||||
wann an des kindes leibe werdent

fraktur2/1487-GartDerGesunthait/0011/010017.gt.txt
daruon vnrain plattern ˖ ¶ Auch
|||||||||||||||||||||||||||||||
daruon vnrain plattern ˖ ¶ Auch

fraktur2/1487-GartDerGesunthait/0011/010018.gt.txt
ſpricht Galienus ˖ Eppich dicker⸗
|||||||||||||||||||||||||||||||||
ſpricht Galienus ˖ Eppich dicker⸗

fraktur2/1487-GartDerGesunthait/0011/010019.gt.txt
mal genützet ˖ iſt die vallend ſucht
||||||||||||||||||||||||||||||||||||
mal genützet ˖ iſt die vallend ſucht

fraktur2/1487-GartDerGesunthait/0011/01001a.gt.txt
bringen ˖ Vnd ſchwanger fraw⸗
||||||||#|-||||||||||||||||||
bringen - ~nd ſchwanger fraw⸗

fraktur2/1487-GartDerGesunthait/0011/01001b.gt.txt
en ſolent nit nützen eppich ˖ wann
||||||||||-#||||||||||||||||||||||
en ſolent ~mt nützen eppich ˖ wann

fraktur2/1487-GartDerGesunthait/0011/01001c.gt.txt
es öffnet die flüſz des vngebornen
|||##|||||||||||||||||||||||||||||
es o◌ͤfnet die flüſz des vngebornen

fraktur2/1487-GartDerGesunthait/0011/01001d.gt.txt
kinds ee es die zeit begreift ˖ ¶ Vñ
|||||||||||||||||||||||||||||-||||##
kinds ee es die zeit begreift~˖ ¶ ſl

fraktur2/1487-GartDerGesunthait/0011/01001e.gt.txt
frawen die kinder ſind ſeügen ſol⸗
||||||||||||||||||||||||||||||||||
frawen die kinder ſind ſeügen ſol⸗

fraktur2/1487-GartDerGesunthait/0011/01001f.gt.txt
len nit nützen eppich ˖ auff das ſie
|||||||||||||||||||||||-||||||||||||
len nit nützen eppich ˖~auff das ſie

fraktur2/1487-GartDerGesunthait/0011/010020.gt.txt
nit vnſinnig werde~nt ˖ o~der die val⸗
||||||||||||||||||+||||#|+||||||||||||
nit vnſinnig werde nt ˖do der die val⸗

fraktur2/1487-GartDerGesunthait/0011/010021.gt.txt
lend ſucht nit v◌̉berkomen ˖ wañ ep
|||||||||||##|||-||||||||---||||||
lend ſucht mrt v~berkomen~~~wañ ep

fraktur2/1487-GartDerGesunthait/0011/010022.gt.txt
pich iſt tempff in das haubt v◌̉ber
|||||||||||||||||||||||||||||--|||
pich iſt tempff in das haubt ~~ber

fraktur2/1487-GartDerGesunthait/0011/010023.gt.txt
ſich bewegen ˖ ¶ Jtem eppich vñ
||||||||||||-||||||||||||||||||
ſich bewegen~˖ ¶ Jtem eppich vñ

fraktur2/1487-GartDerGesunthait/0011/010024.gt.txt
ſtabwurtzel geſoten in laugen da
#|||||||||||||#|||||||||||||||||
ftabwurtzel geloten in laugen da

fraktur2/1487-GartDerGesunthait/0011/010025.gt.txt
uon getzwagẽ ˖ iſt gůt für das har
|||||||||||||--|||||-|||#|||||||||
uon getzwagẽ ~~iſt g~t fur das har

fraktur2/1487-GartDerGesunthait/0011/010026.gt.txt
aus~vallen ˖ genant alopicia ˖
|||+|||||||-#||||||||||||##|-#
aus vallen ~ggenant alopiula~⸗

fraktur2/1487-GartDerGesunthait/0011/010027.gt.txt
wilden eppich ˖
|||||||||||||-#
wilden eppich~⸗

fraktur2/1487-GartDerGesunthait/0011/010028.gt.txt
Capitulum ˖ ˖ vii ˖
|||||||||##|--|||-#
Capitulum-. ~~vii~⸗

fraktur2/1487-GartDerGesunthait/0011/010029.gt.txt
Piũ ſilueſtre latine ˖ ¶ Die
||#|||||||||||||||||||||||||
Piů ſilueſtre latine ˖ ¶ Die

fraktur2/1487-GartDerGesunthait/0011/01002a.gt.txt
maiſter ſprechen das diſes
||||||||||||||||||||||||||
maiſter ſprechen das diſes

fraktur2/1487-GartDerGesunthait/0011/01002b.gt.txt
ſei ain kraut haiſz vñ truck
||||||||||||||||||||||||||||
ſei ain kraut haiſz vñ truck

fraktur2/1487-GartDerGesunthait/0011/01002c.gt.txt
en bey dem dritten grad ˖ ¶ Diſes
|||||||||||||||||||||||||||||||||
en bey dem dritten grad ˖ ¶ Diſes

fraktur2/1487-GartDerGesunthait/0011/01002d.gt.txt
wechſt gern bei dẽ faulen waſſern
||||||||||||||||||||||||||||#||||
wechſt gern bei dẽ faulen wafſern

fraktur2/1487-GartDerGesunthait/0011/01002e.gt.txt
do die fro◌ͤſch wonent ˖ ¶ Auch ne⸗
||||||||||||||||||||||--||||||||||
do die fro◌ͤſch wonent ~~¶ Auch ne⸗

fraktur2/1487-GartDerGesunthait/0011/01002f.gt.txt
nent ettlich diſes apiũ riſus ˖ wañ
||||||||||||||||||||||#||||||||||||
nent ettlich diſes apiü riſus ˖ wañ

fraktur2/1487-GartDerGesunthait/0011/010030.gt.txt
der menſch der diſz nützt in den lei
|||||||||||||||||||||#||||||||||||||
der menſch der diſz nůtzt in den lei

fraktur2/1487-GartDerGesunthait/0011/010031.gt.txt
be der lacht allſo ſeer das er dauon
||||||||||||||||||||||||||||||||||||
be der lacht allſo ſeer das er dauon

fraktur2/1487-GartDerGesunthait/0011/010032.gt.txt
ſtirbt ˖ ¶ Darumb dienet diſz wol
|||||||||||||||||||||||||||||||||
ſtirbt ˖ ¶ Darumb dienet diſz wol

fraktur2/1487-GartDerGesunthait/0011/010033.gt.txt
melancolicis ˖ das iſt ˖ den die kalter
||||||||||||-|||||||||-||||||||||||||||
melancolicis~˖ das iſt~˖ den die kalter

fraktur2/1487-GartDerGesunthait/0011/010034.gt.txt
vnd truckner natur ſind ˖ vnd we⸗
|||||||||||||||||||||||||||||||||
vnd truckner natur ſind ˖ vnd we⸗

fraktur2/1487-GartDerGesunthait/0011/010035.gt.txt
nig freüd habent von natur ˖ vnd
|||||||#||||||||||||||||||||||||
nig freid habent von natur ˖ vnd

fraktur2/1487-GartDerGesunthait/0011/010036.gt.txt
gern mit yn ſelbs redent ˖ Aber vor
|||||||||||||||||||||||||||||||||||
gern mit yn ſelbs redent ˖ Aber vor

fraktur2/1487-GartDerGesunthait/0011/010037.gt.txt
allen dingen rat ich das nit in den
|||||||||||||||||||||||||#|||||||||
allen dingen rat ich das mit in den

fraktur2/1487-GartDerGesunthait/0011/010038.gt.txt
leib zenemẽ ˖ d◌̉ vrſach halb das die
||||||||||||-----|||||||||||||||||||
leib zenemẽ ~~~~~vrſach halb das die

fraktur2/1487-GartDerGesunthait/0011/010039.gt.txt
fro◌ͤſche vnd kroten darauff laich
|||||||||||||||||||||||||||||||||
fro◌ͤſche vnd kroten darauff laich

fraktur2/1487-GartDerGesunthait/0011/01003a.gt.txt
en ˖ vnd andere vergifftige thiere ˖
||||||||||||||||||||||||||||#||#|||#
en ˖ vnd andere vergifftige ihitre ⸗

fraktur2/1487-GartDerGesunthait/0012/010001.gt.txt
¶ Auch iſt diſes kraut von natur
||||||||||||||||||||||||||||||||
¶ Auch iſt diſes kraut von natur

fraktur2/1487-GartDerGesunthait/0012/010002.gt.txt
alſo das ain yeglich vergiftig tier
|||||||||||||||||||||||||||||||||||
alſo das ain yeglich vergiftig tier

fraktur2/1487-GartDerGesunthait/0012/010003.gt.txt
dauõ nit komet es hab ſein natur
||||||||||||||||||||||||||||||||
dauõ nit komet es hab ſein natur

fraktur2/1487-GartDerGesunthait/0012/010004.gt.txt
darauf geworffẽ von freüden vñ
||||||||||||||||||||||||||||||
darauf geworffẽ von freüden vñ

fraktur2/1487-GartDerGesunthait/0012/010005.gt.txt
küczlung ſeines ſamens ¶ Von di
||||||||||||||||||||||-||||||||
küczlung ſeines ſamens~¶ Von di

fraktur2/1487-GartDerGesunthait/0012/010006.gt.txt
ſem kraut beſchreibt vns Diaſco⸗
||||||||||||||||||||||||||||||||
ſem kraut beſchreibt vns Diaſco⸗

fraktur2/1487-GartDerGesunthait/0012/010007.gt.txt
rides vnd ſprichet ˖ das diſes kraut
||||||||||||||||||||||||||||||||||||
rides vnd ſprichet ˖ das diſes kraut

fraktur2/1487-GartDerGesunthait/0012/010008.gt.txt
beneme vnnd haile acrocordines
|||||||||||||||||||#||||||||||
beneme vnnd haile atrocordines

fraktur2/1487-GartDerGesunthait/0012/010009.gt.txt
das ſind lychdorn od◌̉ wa◌ͤrczẽ auff
||||||||||||||||||||-|||-|||||||||
das ſind lychdorn od~ wa~rczẽ auff

fraktur2/1487-GartDerGesunthait/0012/01000a.gt.txt
den zehen an den fu◌ͤſſẽ ¶ Auch nen
||||||||||||||||||||||||||||||||||
den zehen an den fu◌ͤſſẽ ¶ Auch nen

fraktur2/1487-GartDerGesunthait/0012/01000b.gt.txt
nen etlich maiſter diſes porri ˖ Diſz
||||||||||||||||||||||||||||||---||||
nen etlich maiſter diſes porri~~~Diſz

fraktur2/1487-GartDerGesunthait/0012/01000c.gt.txt
kraut zerkniſchet vnd darauf ge⸗
||||||||||#|||||||||||||||||||||
kraut zerkuiſchet vnd darauf ge⸗

fraktur2/1487-GartDerGesunthait/0012/01000d.gt.txt
legt geleich ainem pflaſter ¶ Diſz
||||||||||||||||||||||||||||||||||
legt geleich ainem pflaſter ¶ Diſz

fraktur2/1487-GartDerGesunthait/0012/01000e.gt.txt
krautes ſafft benimet den frawen
||||||||||||||||||||||||||||||||
krautes ſafft benimet den frawen

fraktur2/1487-GartDerGesunthait/0012/01000f.gt.txt
ir geſchwulſt an den brü~ſten dar⸗
||||||||||||||||||||||||+|||||||||
ir geſchwulſt an den brü◌ͤſten dar⸗

fraktur2/1487-GartDerGesunthait/0012/010010.gt.txt
auff gelegt mit eybiſch wurczeln
||||||||||||||||||||||||||||||||
auff gelegt mit eybiſch wurczeln

fraktur2/1487-GartDerGesunthait/0012/010011.gt.txt
¶ Der ſame diſz krautes vermag
||||||||||||||||||||||||||||||
¶ Der ſame diſz krautes vermag

fraktur2/1487-GartDerGesunthait/0012/010012.gt.txt
alle diſe obgeſchribne ſtuck ˖ vnnd
|||||||||||||||||||||||||||||||||||
alle diſe obgeſchribne ſtuck ˖ vnnd

fraktur2/1487-GartDerGesunthait/0012/010013.gt.txt
der ſame iſt nit als ſorgklich zenü⸗
|||||||||-||||||||||||||||||||||||||
der ſame ~ſt nit als ſorgklich zenü⸗

fraktur2/1487-GartDerGesunthait/0012/010014.gt.txt
czen in den leibe / als dann iſt das
-|||||||||||||||||||||||||||||||||||
~zen in den leibe / als dann iſt das

fraktur2/1487-GartDerGesunthait/0012/010015.gt.txt
kraut ¶ Von diſem ſamẽ getrun⸗
|||||||##|||||||||||||||||||||
kraut ¶( on diſem ſamẽ getrun⸗

fraktur2/1487-GartDerGesunthait/0012/010016.gt.txt
cken iſt faſt nücz denẽ die den vier⸗
|||||||||||||||||||||||||||||||||||||
cken iſt faſt nücz denẽ die den vier⸗

fraktur2/1487-GartDerGesunthait/0012/010017.gt.txt
ta◌ͤglchẽ ritten habẽ / den mit wein
|||||||||||||||||||||||||||||||||||
ta◌ͤglchẽ ritten habẽ / den mit wein

fraktur2/1487-GartDerGesunthait/0012/010018.gt.txt
eingenomen / vnd macht wol ha◌ͤr
|||||||||||||||||||||||||||||||
eingenomen / vnd macht wol ha◌ͤr

fraktur2/1487-GartDerGesunthait/0012/010019.gt.txt
men ˖ ¶ Auch benymet der ſamen
||||||||||||||||||||||||||||||
men ˖ ¶ Auch benymet der ſamen

fraktur2/1487-GartDerGesunthait/0012/01001a.gt.txt
die verſtopfung des milczes vnd
|||||||||||||||||||||||||||||||
die verſtopfung des milczes vnd

fraktur2/1487-GartDerGesunthait/0012/01001b.gt.txt
der lebern ˖
||||||||||-#
der lebern~⸗

