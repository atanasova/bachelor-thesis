fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010001.gt.txt
Der Him◌̃liſch Ga◌ͤrtner Lobeſan
|||||||-||||||||||#|||||||||||
Der Him~liſch Ga◌ͤrener Lobeſan

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010002.gt.txt
Kra◌ͤfftig das alles richtet an /
-|||||||||||||||||||||||||||||-|
~ra◌ͤfftig das alles richtet an~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010003.gt.txt
Durchs Wort vnd heylige Sacrament /
||||||||||||||||||||||||||#||||||||
Durchs Wort vnd heylige Saerament /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010004.gt.txt
Daher ſie Chriſten werden gnennt /
||||||||||||||||||##|||||||#||#|-#
Daher ſie Chriſteneverden guenut~t

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010005.gt.txt
Zu guten Wercken wol geſchickt /
||-#||||||||||||||||-||||||||#||
Zu~auten Wercken wol~geſchicke /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010006.gt.txt
Daran man Luſt vnd Frewd erblickt /
||||||||||#||||##|||||||||||||||#||
Daran man ◌ͤuſt yud Frewd erblicke /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010007.gt.txt
Jrn Samen bringens zu rechter zeit /
||||||||||||||||#|||||||||||||||||||
Jrn Samen bringeus zu rechter zeit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010008.gt.txt
Zu dienen jederman bereit /
|#|||||||||||||||||||||||||
Zn dienen jederman bereit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010009.gt.txt
Vnd wie die Pflantzen auff dem Feldt /
|##|||||||||||||||-|||||||||||||||||||
Viw wie die Pflant~en auff dem Feldt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000a.gt.txt
Wenns in das Erdreich ſind geſtellt /
||#||||||||||||||||||||||||||||||||||
Weuns in das Erdreich ſind geſtellt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000b.gt.txt
Viel Vn~gewitter warten auß /
|#|||||+|||||||||||||||||||||
Vtel Vnngewitter warten auß /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000c.gt.txt
Von Schnee / Regen vnnd Windes⸗
||||||||||||||||||||||#|||#||||
Von Schnee / Regen vnns Wiudes⸗

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000d.gt.txt
brauß /
|||||||
brauß /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000e.gt.txt
Alſo diß Pfla◌ͤntzlein Gottes auch
||||||||||||||#|-||||||||||||||||
Alſo diß Pfla◌ͤut~lein Gottes auch

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/01000f.gt.txt
Viel leiden mu◌ͤſſen / iſt der Brauch
|||||#||||||||||||||||||||||||||||||
Viel ſeiden mu◌ͤſſen / iſt der Brauch

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010010.gt.txt
Jn dieſer Welt / biß durch den Todt
|#|||||||||||#||||||||||||||||||||#
Ju dieſer Wele / biß durch den Tode

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010011.gt.txt
Sie all hinnimpt der liebe Gott /
||||||||||||||||||||||||||||||#||
Sie all hinnimpt der liebe Gote /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010012.gt.txt
Vnd wirfft ſie in die Erd hinnein /
|||||||||||||||||||||||||||||||||-|
Vnd wirfft ſie in die Erd hinnein~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010013.gt.txt
Gerad wie in ein Schlaffbettlein /
||||||||||||-|||||||||||||||||||||
Gerad wie in~ein Schlaffbettlein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010014.gt.txt
Zu ruhen biß an Ju◌ͤngſten tag /
|#|||||||||||||||||||||||||||-|
Zn ruhen biß an Ju◌ͤngſten tag~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010015.gt.txt
An welchen er Sie / Jch dir ſag /
||-||||||||||||||||||||||||||||||
An~welchen er Sie / Jch dir ſag /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010016.gt.txt
Gleich wie die Pfla◌ͤntzlein Sommers zeit /
|||||||||||||||||||||-||||||||||||||||||||
Gleich wie die Pfla◌ͤn~zlein Sommers zeit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0037/010017.gt.txt
Herfu◌ͤr la◌ͤſt keynen weit vnd breit /
|||||||||||||||||||||||||||||||||||||
Herfu◌ͤr la◌ͤſt keynen weit vnd breit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010001.gt.txt
Zu blu◌ͤhen vnd zu wachſen fein /
||||||||||||||||||||||||||||||||
Zu blu◌ͤhen vnd zu wachſen fein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010002.gt.txt
Bey Gott vnd ſeinen Engelein /
||||||||||||||||||||||||||||||
Bey Gott vnd ſeinen Engelein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010003.gt.txt
Jn ewiger Frewd vnd Herrligkeit /
|||||||||||||||||||||||||||||||||
Jn ewiger Frewd vnd Herrligkeit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010004.gt.txt
Deß danckt jhm alle Chriſtenheit.
||||||||||||||-||||||||||||||||||
Deß danckt jhm~alle Chriſtenheit.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010005.gt.txt
ter / ſo vns erjnnern deß Ge⸗
|||||||||||||||#|||||||||||||
ter / ſo vns erinnern deß Ge⸗

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010006.gt.txt
ſatzes / ſeiner Krafft vnd
||||||||||||||||||||||||||
ſatzes / ſeiner Krafft vnd

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010007.gt.txt
Natur / vnd der
#||||||||||||||
Matur / vnd der

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010008.gt.txt
Buß.
||||
Buß.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010009.gt.txt
Aron.
|||||
Aron.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000a.gt.txt
PSAL. CXXXIII.
|#|#||--#####|
P At. ~~0 11t.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000b.gt.txt
Sihe / wie fein vñ lieblich iſt es / daß Bru◌ͤ⸗
|#||||||||||||||||||||#||||||||||||||||#||||-|
Slhe / wie fein vñ lieslich iſt es / daſ Bru~⸗

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000c.gt.txt
der eintra◌ͤchtig bey einander wohnen /
||||||||||||||||||||||||||||||||||||||
der eintra◌ͤchtig bey einander wohnen /

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000d.gt.txt
wie der ko◌ͤſtlich~ Balſam iſt / der vom
||||||||#||||||#|+|||||||||||||||||||#|
wie der ro◌ͤſtliehz Balſam iſt / der vvm

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000e.gt.txt
Haupt Aaron herab fleuſſt / inn ſein
|||#||||||||||||||||||||||||||||||||
Hauvt Aaron herab fleuſſt / inn ſein

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/01000f.gt.txt
gantzen Bart / der herab fleuſſt in ſein
||||||||||||||||||||||||||||||||||||||||
gantzen Bart / der herab fleuſſt in ſein

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010010.gt.txt
Kleydt / rc.
#||||||||-||
Aleydt / ~c.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010011.gt.txt
PSAL. CVI.
-###||###|
~F2 . v t.

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010012.gt.txt
Sie empo◌ͤreten ſich aber wider Moiſen
||||||||-||||||||||||||||||||||||||||
Sie empo~reten ſich aber wider Moiſen

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010013.gt.txt
im La◌ͤger / wider Aron den He~yligen
|||||||||||||||||||||||||||||+||||||
im La◌ͤger / wider Aron den He yligen

fraktur2/1588-Paradeißgärtlein-Rosbach/0038/010014.gt.txt
deß H~ERRN / Num. 16.
||#||+#|###||||||||##
den Hv R RM/ Num. 1⸗-

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010001.gt.txt
E~~SAIAE II.
|++#|#|#|##|
E 2 A At 1t.

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010002.gt.txt
Kompt laſt vns auff den Bergk deß
#|||||||||||||||||||||||||||#|||#
Rompt laſt vns auff den Bergd deſ

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010003.gt.txt
H~ER~RE~N gehen / zum Hauß deß
|+#|+||+#|||||||||||||||||||||
Hw RdRE M gehen / zum Hauß deß

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010004.gt.txt
Gottes Jacob / daß er vns le~hre ſeine
||||||||||||||||||||||||||||+|||||||||
Gottes Jacob / daß er vns le hre ſeine

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010005.gt.txt
Wege / vnd wir wandlen auff ſeinen
#|||||||||||||||||||||||||||||||||
Vege / vnd wir wandlen auff ſeinen

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010006.gt.txt
Ste~gen / denn von Zion wirdt das
|#|+|||||||||||||||#|||||||||||||
See gen / denn von gion wirdt das

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010007.gt.txt
Geſetz außgehen / vñ deß HERRN
||#||||||#||#||||||#|#||||-###
Gefetz aufgeben / vs teß H~A M

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010008.gt.txt
W~ort von Hieruſalem.
|+##|||||||||||||||||
WVgzt von Hieruſalem.

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010009.gt.txt
Leibliche Wirckung.
|||#|||||||||||||||
Leitliche Wirckung.

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000a.gt.txt
Jß Edle Kraut vnnd Wurtz
|||||#||||||||||||||||-|
Jß Edie Kraut vnnd Wur~z

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000b.gt.txt
fu◌ͤr Gifft (geſtifft /
|||||||-#|||||||||||||
fu◌ͤr Gi~ſt (geſtifft /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000c.gt.txt
Von Gott dem HERREN iſt
|||||||||||-||-####||||
Von Gott de~ H~etam iſt

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000d.gt.txt
Drumb in der Noth mans brauchen ſol /
|||||||||||||||||||||||||||||||||||||
Drumb in der Noth mans brauchen ſol /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000e.gt.txt
Zur Peſtilentz es dienet wol /
|||||||||||||-||||||||||||||-|
Zur Peſtilent~ es dienet wol~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/01000f.gt.txt
So man den Safft mit Eſſig trinckt /
||||||||||||||||||||||||||||||||||||
So man den Safft mit Eſſig trinckt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010010.gt.txt
Denſelben Krancken wolgelingt /
||#||||||||||||||||||||||||||||
Deuſelben Krancken wolgelingt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010011.gt.txt
Sein Bla◌ͤtter mit Saltz vermiſcht eben /
||||||||||||||||||||||||||||||||||||||||
Sein Bla◌ͤtter mit Saltz vermiſcht eben /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010012.gt.txt
Thut dir dazu ein Auffſchlag geben /
|||||||||||||||||||##|||||||||||||||
Thut dir dazu ein Anſfſchlag geben /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010013.gt.txt
Fu◌ͤrn bo◌ͤſen Lufft auch Arou iſt /
||||||||||||||||||||||||||||||||-|
Fu◌ͤrn bo◌ͤſen Lufft auch Arou iſt~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010014.gt.txt
Behu◌ͤt fu◌ͤr Gifft / ſoll ſeyn gewiß /
|||||||||||||||-|||||||||||||||||||-|
Behu◌ͤt fu◌ͤr Gif~t / ſoll ſeyn gewiß~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010015.gt.txt
Die Wu~rtzel ſiede mit Honig wol /
||||||+|||||||||||||||||||||||||||
Die Wuirtzel ſiede mit Honig wol /

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010016.gt.txt
Trincks ein / dauon das Feber ſol
||||||||||-||||||||||||||||||||||
Trincks ei~ / dauon das Feber ſol

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010017.gt.txt
Vertrieben werden / auch deß gleich
|||||||||||||||||||||||||||||||||||
Vertrieben werden / auch deß gleich

fraktur2/1588-Paradeißgärtlein-Rosbach/0039/010018.gt.txt
Deß~ Magens Wuſt von Arm vñ Reich /
|||+#||||||||||||||||||||||||||||-|
Deßd◌ͤMagens Wuſt von Arm vñ Reich~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010001.gt.txt
Die Wurtz geſotten in dem Wein /
||||||||-||||#||||||||||||||||||
Die Wurt~ geſetten in dem Wein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010002.gt.txt
Mit Stahl ſoll abgeleſchet ſeyn /
||#||||||||||||||||||||||||||||-|
Mie Stahl ſoll abgeleſchet ſeyn~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010003.gt.txt
Zum andern vnd zum dritten ma~l /
|||||||||||||||||||||||||||||+|||
Zum andern vnd zum dritten ma l /

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010004.gt.txt
Vertreibt getruncken d Magens Qual /
||||||||||||||||||||||-|||||||||||||
Vertreibt getruncken d~Magens Qual /

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010005.gt.txt
Ein Pflaſter bereit mit dieſem Safft /
#||||||||||||||||||||||||||||||||-#|||
Gin Pflaſter bereit mit dieſem Sa~it /

fraktur2/1588-Paradeißgärtlein-Rosbach/0040/010006.gt.txt
Mit Zwibel Schmaltz wirt abgeſchafft /
|||-||||||-|||||||||||||||||||||||||||
Mit~Zwibel~Schmaltz wirt abgeſchafft /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010001.gt.txt
Ein Geſchwer / ſo man Feigsblatter neñt /
-||||||||||||||||||||||||||||||||||||#|||
~in Geſchwer / ſo man Feigsblatter neſt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010002.gt.txt
Jſt manchem leyd / daß er ſolch kennt /
||||||#|||||||||||||||||||||||||||##|||
Jſt mauchem leyd / daß er ſolch kemit /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010003.gt.txt
Die Bla◌ͤtter legt man auff die Ka◌ͤß /
|#|||||||||||||||||||-||#||||||||||||
Dte Bla◌ͤtter legt man~auſf die Ka◌ͤß /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010004.gt.txt
Vertreibt darvon das Maden Gefreß.
||||||||||||||||||||||||||||||||||
Vertreibt darvon das Maden Gefreß.

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010005.gt.txt
Geiſtliche Wirckung.
||||||||||||||||||||
Geiſtliche Wirckung.

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010006.gt.txt
~~EJn ſcharpffen Gſchmack wie hat Aron /
++#||||||||||||||||||||||-||||||||||||||
gd Jn ſcharpffen Gſchmack~wie hat Aron /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010007.gt.txt
Vud ſich mit ſeiner Trauben ſchon
|||||||||||||||||||||||||||||||||
Vud ſich mit ſeiner Trauben ſchon

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010008.gt.txt
Erzeiget auß der maſſen fein /
|||||||||||||||||||#||||||||||
Erzeiget auß der mafſen fein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010009.gt.txt
Alſo das Gſatz durchauß gmein /
|||||||||||||||||||||||-|||||||
Alſo das Gſatz durchauß~gmein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000a.gt.txt
Dergleichen Art / Natur vnd Krafft /
|||||||||||||||||||||||||||||||--##|
Dergleichen Art / Natur vnd Kra~~ſe/

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000b.gt.txt
Hat in ſich / vnd ſolch Eigenſchafft /
|||||--|||||||||||||||||||||||||||||||
Hat i~~ſich / vnd ſolch Eigenſchafft /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000c.gt.txt
Denn wie Aron genommen eyn /
||###|||||||||||||||||||||||
Deurmwie Aron genommen eyn /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000d.gt.txt
Hart brennet auff der Zungen dein /
|||||||||||||||||-|||||||||||||||||
Hart brennet auff~der Zungen dein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000e.gt.txt
Alſo brennt auch / gleich wie ein Feuwer /
||||||||#|||||||||||||||||||||||||||||||||
Alſo bremnt auch / gleich wie ein Feuwer /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/01000f.gt.txt
Diß Lehr im Hertzen vngeheuwer /
|#||||||||||||||||||||||||||||||
Dtß Lehr im Hertzen vngeheuwer /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010010.gt.txt
Wenn man dardurch die Su◌ͤnd auffdeckt /
||-##||||||||||||||||||||#|||||||||||||
We~uiman dardurch die Su◌ͤud auffdeckt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010011.gt.txt
Die bo◌ͤſen Gwiſſen hefftig ſchreckt /
|||||||||||||||||||||||||||||||||||||
Die bo◌ͤſen Gwiſſen hefftig ſchreckt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010012.gt.txt
Vnd macht den Menſchen angſt vnd bang /
|#|||||||||||#|||||||#||#|#|||||-||||||
Vud macht denſMenſcheu augft vnd~bang /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010013.gt.txt
Daß Zeit vnd Weyl jhm wird zulang /
||||||||||||||||||||||||||||#||||||
Daß Zeit vnd Weyl jhm wird znlang /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010014.gt.txt
Denn es gebeut ſo hohe ding /
||--#||||||||||||||||||||||-|
De~~mes gebeut ſo hohe ding~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010015.gt.txt
Die v~ns nicht mu◌ͤglich zhalten ſind /
|||||+#|||||||||||||||||||||||||||||||
Die vurs nicht mu◌ͤglich zhalten ſind /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010016.gt.txt
Welch aber deß ſich vnterſtehn /
|||||||#||||||||||||||||||||||||
Welch aoer deß ſich vnterſtehn /

fraktur2/1588-Paradeißgärtlein-Rosbach/0041/010017.gt.txt
Ohn Heuchley wirdts nicht abgehn /
||||||||||||||||||||||||||||||||||
Ohn Heuchley wirdts nicht abgehn /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010001.gt.txt
Erjnnern dich~ die Bla◌ͤtter groß /
||##|||||||||+|||||||-||||||||||||
Eriunern dichh die Bl~◌ͤtter groß /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010002.gt.txt
An dieſer Trauben alſo bloß /
|||||||||||||||||||||||||||-|
An dieſer Trauben alſo bloß~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010003.gt.txt
Sampt dieſes Kra◌ͤutleins Trauben rot /
||||||||||||||||||||||||||||||||||||||
Sampt dieſes Kra◌ͤutleins Trauben rot /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010004.gt.txt
Wie ſie allda gemalet ſtoht /
|||||||||||||||||||||||||||||
Wie ſie allda gemalet ſtoht /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010005.gt.txt
Allein zum ſchein die Frucht iſt ring /
|||||||||||||||||||||||||||||||||||||||
Allein zum ſchein die Frucht iſt ring /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010006.gt.txt
Alſo die Heuchler mit geding /
|||||||||||#||||#|||||||||||||
Alſo die Henchlet mit geding /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010007.gt.txt
Durch Gſatzes Werck / erzeigen fein
||||||||||-||||||||||||||||||||||||
Durch Gſat~es Werck / erzeigen fein

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010008.gt.txt
J~r Heiligkeit vnd groſſen Schein /
|+||||||||#||||||||||||||||||||||||
JJr Heiligteit vnd groſſen Schein /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010009.gt.txt
Da~mit den Himmel verdienen wo◌ͤlln /
||+#||||||||||||||||||||||||||||-|||
Dainit den Himmel verdienen wo◌ͤl~n /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000a.gt.txt
Vnd mit jrn Kra◌ͤfften das Gſatz erfu◌ͤlln /
|||||||||||-||||||||||||||||||-||||||||-#|
Vnd mit jrn~Kra◌ͤfften das Gſat~ erfu◌ͤll~u/

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000b.gt.txt
Welchs jn / ſag ich / doch nimmermehr
||||||||#|||||||-||||||||||||||||||||
Welchs ju / ſag ~ch / doch nimmermehr

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000c.gt.txt
Zu thun iſt mu◌ͤglich / alſo die Ehr
|||||||||||||||||||||||||||||||||||
Zu thun iſt mu◌ͤglich / alſo die Ehr

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000d.gt.txt
Chriſto dem H~~~ERREN nemmen hin /
|||||||||||||+++#####||||#||-|||||
Chriſto dem H erw asr nemnen~hin /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000e.gt.txt
Vnd ſchreibens zu jrem Gewinn /
|-|||||||||||||||||||||||||#|-|
V~d ſchreibens zu jrem Gewimn~/

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/01000f.gt.txt
Sein Verdienſt nit gnugſam ſeyn erkenn /
|||||||||||#||||||||#|||||||||||||||-##|
Sein Verdiemſt nit guugſam ſeyn erke~mu/

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010010.gt.txt
Fu◌ͤr Gott damit nur zu beſtehn /
||||||||||||||||||||||||||||||||
Fu◌ͤr Gott damit nur zu beſtehn /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010011.gt.txt
Das ſind Heuchler / vnd bleibens auch /
|||||||||||||||||||||#|||||||||||||||||
Das ſind Heuchler / vud bleibens auch /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010012.gt.txt
Ja Gleißner ſag ich / nach gebrauch
||||||||||||||||||||||||||||||||#||
Ja Gleißner ſag ich / nach gebranch

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010013.gt.txt
Der Phariſeer / Chriſtus meldt / Matt. 23.
||||#||||||||||||||||||||||||||||||-#|---|
Der Ohariſeer / Chriſtus meldt / Ma~u.~~~.

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010014.gt.txt
Jm Mattheo ſichs ſo verhelt /
||-||||||||||||||||||||||||||
Jm~Mattheo ſichs ſo verhelt /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010015.gt.txt
Beym Luca auch am ſechſten ſpricht /
|||||#||||||||||||||||||||||||||||||
Beym ◌ͤuca auch am ſechſten ſpricht /

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010016.gt.txt
Wo der Ju◌ͤnger Gerechtigkeit ſeye nicht
||||||||||||||||||||||||#||||||||||||||
Wo der Ju◌ͤnger Gerechtigteit ſeye nicht

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010017.gt.txt
Viel beſſer denn des Schrifftgelehrts
|#||||||||||||#||||||||||||||||||||||
Vtel beſſer deun des Schrifftgelehrts

fraktur2/1588-Paradeißgärtlein-Rosbach/0042/010018.gt.txt
Vnd Phariſeer / alſo bewerts /
||||||||||||||||||||||||||||||
Vnd Phariſeer / alſo bewerts /

