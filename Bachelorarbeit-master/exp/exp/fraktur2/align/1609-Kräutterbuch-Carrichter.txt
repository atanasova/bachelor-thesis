fraktur2/1609-Kräutterbuch-Carrichter/0025/010001.gt.txt
tige flu◌ͤß / allein im ſchatten gedo◌ͤrret / vnd
||||||||||||||||||||||||||||||||||||||-||||||||
tige flu◌ͤß / allein im ſchatten gedo◌ͤr~et / vnd

fraktur2/1609-Kräutterbuch-Carrichter/0025/010002.gt.txt
ein blat auff den ſchaden gelegt / ſo zeucht
||||||||||||||||||||||||||||#||||||||||||||#
ein blat auff den ſchaden geiegt / ſo zeuchs

fraktur2/1609-Kräutterbuch-Carrichter/0025/010003.gt.txt
es alle Gallſu◌ͤchtige Flu◌ͤß vnd ding her⸗
|||||||||||||||||||||||||||||||||||||||||
es alle Gallſu◌ͤchtige Flu◌ͤß vnd ding her⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/010004.gt.txt
auß / laßt auch keinen Fluß nicht ſtecken /
||||||||||||||||||||||||#||||||||||||||||||
auß / laßt auch keinen Fiuß nicht ſtecken /

fraktur2/1609-Kräutterbuch-Carrichter/0025/010005.gt.txt
der von der Gallen ein Vrſprung hat /
|||||||||||||||||||||||||||||||||||||
der von der Gallen ein Vrſprung hat /

fraktur2/1609-Kräutterbuch-Carrichter/0025/010006.gt.txt
oder eine einige Hitz in ſich / Abends vnd
||||||||||||||||||||||||||||||||||||||||||
oder eine einige Hitz in ſich / Abends vnd

fraktur2/1609-Kräutterbuch-Carrichter/0025/010007.gt.txt
Morgens ein blat auff den ſchadẽ gelegt.
||||||||||||||||||||||||||||||||||||||||
Morgens ein blat auff den ſchadẽ gelegt.

fraktur2/1609-Kräutterbuch-Carrichter/0025/010008.gt.txt
Dieſe ding alle / ſo in diſem grad ſtehn /
||||||||||||||||||||||||||||||||||||||||||
Dieſe ding alle / ſo in diſem grad ſtehn /

fraktur2/1609-Kräutterbuch-Carrichter/0025/010009.gt.txt
ſafftig in die Schuh gelegt / zeucht Geel⸗
||||||||||||||||||||||||||||||||||||||||#|
ſafftig in die Schuh gelegt / zeucht Geei⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000a.gt.txt
ſu◌ͤchtig Waſſer zun Solen herauß / alſo
|||||||||||||||||||||||||||||||||||||||
ſu◌ͤchtig Waſſer zun Solen herauß / alſo

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000b.gt.txt
hefftig / das es auch die Waſſerſu◌ͤchtigen
||||||||||||||||||||||||||||||||||||||||||
hefftig / das es auch die Waſſerſu◌ͤchtigen

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000c.gt.txt
hilfft / vnd erlo◌ͤßt / Allein die Lehnenblet⸗
|||||||||||||||||||||||||||||||||||||||||||||
hilfft / vnd erlo◌ͤßt / Allein die Lehnenblet⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000d.gt.txt
ter vnd blu◌ͤet / ſolt man etwas zuſetzen von
||||||||||||||||||||||||||||||||||||||||||||
ter vnd blu◌ͤet / ſolt man etwas zuſetzen von

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000e.gt.txt
Rettichſaf~t / oder Merrettich / ſonſt zeucht
||||||||||+||||||||-|||||||||||||||||||||||||
Rettichſafft / oder~Merrettich / ſonſt zeucht

fraktur2/1609-Kräutterbuch-Carrichter/0025/01000f.gt.txt
er blatern. Das thut auch Camepitis
||||||||||||||||||||||||||||||||||#
er blatern. Das thut auch Camepitin

fraktur2/1609-Kräutterbuch-Carrichter/0025/010010.gt.txt
lignola, auch die jungen ſchoß von den
|||||||#||||||||||||||||||||||||||||||
lignola⸗ auch die jungen ſchoß von den

fraktur2/1609-Kräutterbuch-Carrichter/0025/010011.gt.txt
edlen weiſſen Weinreben / Vnd ſo man
||||||||||#|||||||||||||||||||||||||
edlen weiſfen Weinreben / Vnd ſo man

fraktur2/1609-Kräutterbuch-Carrichter/0025/010012.gt.txt
dieſe drey ding / als Weinreben / Lehnen /
||||||||||||||||||||||||||||||||||||||||||
dieſe drey ding / als Weinreben / Lehnen /

fraktur2/1609-Kräutterbuch-Carrichter/0025/010013.gt.txt
Cam~epitis, zu a◌ͤſchen brennet / vnd deren
|||+||||||||||||||||||||||||||||||||||||||
Cam epitis, zu a◌ͤſchen brennet / vnd deren

fraktur2/1609-Kräutterbuch-Carrichter/0025/010014.gt.txt
a◌ͤſche zweyer Hu◌ͤner eyer dick in Tuch ge⸗
|-||||||||||||||||||||||||||||||||||||||||
a~ſche zweyer Hu◌ͤner eyer dick in Tuch ge⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/010015.gt.txt
bunden / in Wein gelegt / ſo digerierts ta◌ͤg⸗
|||||||||||-||||||||||||--|||||||||||||||||||
bunden / in~Wein gelegt ~~ſo digerierts ta◌ͤg⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/010016.gt.txt
lich / vnd treibt die Waſſerſucht von ei⸗
|||||||||||||||||||||||||||||||||||||||||
lich / vnd treibt die Waſſerſucht von ei⸗

fraktur2/1609-Kräutterbuch-Carrichter/0025/010017.gt.txt
nem Menſchen.
|||||||||||||
nem Menſchen.

fraktur2/1609-Kräutterbuch-Carrichter/0025/010018.gt.txt
Jn dem Zwilling oder in der Wage /
||||||||||||||||||||||||||||||||||
Jn dem Zwilling oder in der Wage /

fraktur2/1609-Kräutterbuch-Carrichter/0026/010001.gt.txt
ſeind auch ding / die es thun im letzten
||||||||||||||||||||||||||||||||||||||||
ſeind auch ding / die es thun im letzten

fraktur2/1609-Kräutterbuch-Carrichter/0026/010002.gt.txt
grad / ſeind auch viel ding / die die Waſſer⸗
||||||||||||-||||||||||||||||||||||||||||||||
grad / ſeind~auch viel ding / die die Waſſer⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010003.gt.txt
ſucht alſo hin verzehren / als vnzeitig Ca⸗
|||||||||||||||||||||||||||||||||||||||||||
ſucht alſo hin verzehren / als vnzeitig Ca⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010004.gt.txt
millenkraut zu a◌ͤſchen gebrant / vnd alſo
|||||||||||||||||||||||||||||||||||||||||
millenkraut zu a◌ͤſchen gebrant / vnd alſo

fraktur2/1609-Kräutterbuch-Carrichter/0026/010005.gt.txt
gebraucht / Auch vnzeitige Reinblumen /
|||||||||||||||||||||||||||||||||||||||
gebraucht / Auch vnzeitige Reinblumen /

fraktur2/1609-Kräutterbuch-Carrichter/0026/010006.gt.txt
Tanacetum genandt / auch der Bocks⸗
|||||||||||||||||||||||||||||||||||
Tanacetum genandt / auch der Bocks⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010007.gt.txt
bart / auch der gemeine Daurant / welcher
|||||||||||||||||||||||||||||||||||||||||
bart / auch der gemeine Daurant / welcher

fraktur2/1609-Kräutterbuch-Carrichter/0026/010008.gt.txt
in den erſten grad geho◌ͤrt / vnd derglei⸗
|||||||||||||||||||||||-|||||||||||||||||
in den erſten grad geho~rt / vnd derglei⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010009.gt.txt
chen viel / allein in Wein gelegt ſo rohe /
|||||||||||||||||||||||||||||||||||||||||||
chen viel / allein in Wein gelegt ſo rohe /

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000a.gt.txt
thun ſie allgemach die Waſſerſucht hin⸗
#||||||||||||||||||||||||||||||||||||||
ehun ſie allgemach die Waſſerſucht hin⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000b.gt.txt
weg / welche von einem Hertzklopffen
||||||||||||||||||||||||||||||||||||
weg / welche von einem Hertzklopffen

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000c.gt.txt
kompt / das ſeind Blutwaſſerſucht / auch
||||||||||||||||||||||||||||||||||||||||
kompt / das ſeind Blutwaſſerſucht / auch

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000d.gt.txt
Roßmiſt zu a◌ͤſchen gebrandt / vnd alſo ge⸗
||||||||||-|||||||||||||||||||||||||||||||
Roßmiſt zu~a◌ͤſchen gebrandt / vnd alſo ge⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000e.gt.txt
braucht im Wein / thut die Blut waſſer⸗
|||||||||||||||||||||||||||||||||||||||
braucht im Wein / thut die Blut waſſer⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/01000f.gt.txt
ſucht / welche von der Cholera kompt /
||||||||||||||||||||||||||||||||||||||
ſucht / welche von der Cholera kompt /

fraktur2/1609-Kräutterbuch-Carrichter/0026/010010.gt.txt
hinweg. Alſo werden dieſe ding in den er⸗
|||||||||||||||||||||||||||||||||||||||||
hinweg. Alſo werden dieſe ding in den er⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010011.gt.txt
ſten grad geſetzt / in welchem der Geiſt
||||||||||||||||||||||||||||||||||||||||
ſten grad geſetzt / in welchem der Geiſt

fraktur2/1609-Kräutterbuch-Carrichter/0026/010012.gt.txt
vberflu◌ͤßig iſt / vnd die Materi zugrob /
|||||||||||||||||||||||||||||||||||||||||
vberflu◌ͤßig iſt / vnd die Materi zugrob /

fraktur2/1609-Kräutterbuch-Carrichter/0026/010013.gt.txt
vnd deſſelbenhalben iſt jhr Geiſt durch⸗
||||||||||||||||||||||||||||||||||||||||
vnd deſſelbenhalben iſt jhr Geiſt durch⸗

fraktur2/1609-Kräutterbuch-Carrichter/0026/010014.gt.txt
tringent / zeucht alle Geſchwulſt / vnnd
||||||||||||||||||||||||||||||||||||||||
tringent / zeucht alle Geſchwulſt / vnnd

fraktur2/1609-Kräutterbuch-Carrichter/0026/010015.gt.txt
Gifft hin. Aber die im erſten grad deß
|#||||||||||||||||||||||||||||||||||||
Gefft hin. Aber die im erſten grad deß

fraktur2/1609-Kräutterbuch-Carrichter/0026/010016.gt.txt
Schu◌ͤtzens ſtehn / thun die Waſſerſucht
||||||||||||||||||||||||||||||||||||||#
Schu◌ͤtzens ſtehn / thun die Waſſerſucho

fraktur2/1609-Kräutterbuch-Carrichter/0026/010017.gt.txt
hin / ſo von einer Geelſucht oder Tertian
|||||||||||||||||||||||||||||||||||||||||
hin / ſo von einer Geelſucht oder Tertian

fraktur2/1609-Kräutterbuch-Carrichter/0026/010018.gt.txt
Feiber kommen.
#|||||-|||||||
Veiber~kommen.

fraktur2/1609-Kräutterbuch-Carrichter/0027/010001.gt.txt
Es iſt die gro◌ͤſte Tugend die Gótt den
|||||||||||||||||||||||||||||||#||-|||
Es iſt die gro◌ͤſte Tugend die Gott~den

fraktur2/1609-Kräutterbuch-Carrichter/0027/010002.gt.txt
~Menſchen geben hat / in den zweyen er⸗
+||||||||||||||||||||||||||||||||||||||
fMenſchen geben hat / in den zweyen er⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010003.gt.txt
ſten graden deß Geiſtlichen Zeichens / vnd
|||||||||||||||-||||||||||||||||||||||||||
ſten graden deß~Geiſtlichen Zeichens / vnd

fraktur2/1609-Kräutterbuch-Carrichter/0027/010004.gt.txt
in den letzten zweyen graden deß Ant pa⸗
||||||||||||||||||||||||||||||||||||||||
in den letzten zweyen graden deß Ant pa⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010005.gt.txt
tiſchen Zeichens / Dann dieſe ding haben
||||||||||||||||||||||||||||||||||||||||
tiſchen Zeichens / Dann dieſe ding haben

fraktur2/1609-Kräutterbuch-Carrichter/0027/010006.gt.txt
einen perfecten Geiſt / die in den zweyen
|||||||||||#||||||||||||||||||||||||||#||
einen perfeeten Geiſt / die in den zwenen

fraktur2/1609-Kräutterbuch-Carrichter/0027/010007.gt.txt
erſten graden ſtehen / damit ſie vberflu◌ͤßi~ge
|||||||||||||||||||||||||||||||||||||||##||+#|
erſten graden ſtehen / damit ſie vberfli ßi ze

fraktur2/1609-Kräutterbuch-Carrichter/0027/010008.gt.txt
Materi an ſich ziehen / vnd digerieren vn⸗
||||||||||||||||||||||||||||||||||||||||||
Materi an ſich ziehen / vnd digerieren vn⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010009.gt.txt
der ſich / darnach der Gewalt der Materi
||||||||||||||||||||||||||||||||||||||||
der ſich / darnach der Gewalt der Materi

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000a.gt.txt
iſt / ſo laſſen ſie nichts ſtecken / ſond~ern
|||||||||||||||||||||||||||||||||||||||||+#||
iſt / ſo laſſen ſie nichts ſtecken / ſond. rn

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000b.gt.txt
nemmen allen vberfluß Materialiſch mit
||||||||||||||||||||||||||||||||||||||
nemmen allen vberfluß Materialiſch mit

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000c.gt.txt
jhrem vollkommenen Geiſt hinweg / rei⸗
#|||||||||||||||||||||||||||||||||||||
ſhrem vollkommenen Geiſt hinweg / rei⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000d.gt.txt
nigen alſo die ſcha◌ͤden / vnd alles was von
|||||||||||||||||||||||||||||||||||||||||||
nigen alſo die ſcha◌ͤden / vnd alles was von

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000e.gt.txt
Seelſu◌ͤchtigen Materialiſchen Flu◌ͤſſen
#||#||||||||||||||||||||||||||||||||||
Geeiſu◌ͤchtigen Materialiſchen Flu◌ͤſſen

fraktur2/1609-Kräutterbuch-Carrichter/0027/01000f.gt.txt
vorhanden iſt / in ſcha◌ͤden vnd am Leib.
||||||||||||||||||||||||||||||||||||||||
vorhanden iſt / in ſcha◌ͤden vnd am Leib.

fraktur2/1609-Kräutterbuch-Carrichter/0027/010010.gt.txt
Darum◌̃ der rechte grund aller weichung /
|||||#||||||||||||||||||||||||||||||||||
Darumñ der rechte grund aller weichung /

fraktur2/1609-Kräutterbuch-Carrichter/0027/010011.gt.txt
zeitigung in den zweyen erſten graden ſte⸗
||||||||||||||||||||||||||||||||||||||||||
zeitigung in den zweyen erſten graden ſte⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010012.gt.txt
het deß Geiſtes / doch ein jedes nach ſeiner
||||||||||||||||||||||||||||||||||||||||||||
het deß Geiſtes / doch ein jedes nach ſeiner

fraktur2/1609-Kräutterbuch-Carrichter/0027/010013.gt.txt
art der Feuchtigkeit / Aber auß den an⸗
|||||||||||||||||||||||||||||||||||||||
art der Feuchtigkeit / Aber auß den an⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010014.gt.txt
dern zweyen graden deß dritten vnd vier⸗
||||||||||||||||||||||||||||||||||||||||
dern zweyen graden deß dritten vnd vier⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010015.gt.txt
ten deß Harmoniſchen Zeichens ſollen
||||||||||||||||||||||||||||||||||||
ten deß Harmoniſchen Zeichens ſollen

fraktur2/1609-Kräutterbuch-Carrichter/0027/010016.gt.txt
die Wundtra◌ͤncke gemacht werden / wie⸗
||||||||||||||||||||||||||||||||||||||
die Wundtra◌ͤncke gemacht werden / wie⸗

fraktur2/1609-Kräutterbuch-Carrichter/0027/010017.gt.txt
wol ſie auch groſſe weichung geben / aber
|||||||||||||||||||||||||||||||||||||||||
wol ſie auch groſſe weichung geben / aber

fraktur2/1609-Kräutterbuch-Carrichter/0027/010018.gt.txt
jhr Geiſt viel bequemer durch Tra◌ͤnck /
|||||||||||||||||#|||||||||||||||||||||
jhr Geiſt viel beguemer durch Tra◌ͤnck /

fraktur2/1609-Kräutterbuch-Carrichter/0028/010001.gt.txt
dann der Geiſt durchtringet vnd ſcheidet
|||||||||||||||||||||||||||||||||||||||#
dann der Geiſt durchtringet vnd ſcheidee

fraktur2/1609-Kräutterbuch-Carrichter/0028/010002.gt.txt
alles ſauber vnd reines von dem vnſau⸗
||||||||||||||||||||||||||||||||||||||
alles ſauber vnd reines von dem vnſau⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/010003.gt.txt
bern vnd vnreinen / Aber im vierten grad
||||||||||||||||||||||||||||||||||||||||
bern vnd vnreinen / Aber im vierten grad

fraktur2/1609-Kräutterbuch-Carrichter/0028/010004.gt.txt
deß Schu◌ͤtzens purgieren etwas / vnd ſon⸗
|||||||||#|||||||||#|||||||||||||||||||||
deß Schu◌ͤnzens purgteren etwas / vnd ſon⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/010005.gt.txt
derlich all Metall die darinnen erfunden
||||||||||||||||||||||-|||||||||||||||||
derlich all Metall die~darinnen erfunden

fraktur2/1609-Kräutterbuch-Carrichter/0028/010006.gt.txt
werden / wie das ordenlich nach einander
||||||||||||||||||||||||||||||||||||||||
werden / wie das ordenlich nach einander

fraktur2/1609-Kräutterbuch-Carrichter/0028/010007.gt.txt
erzelt wird werden. Dann der vierte grad
||||||||||||||||||||||||||||||||||||||||
erzelt wird werden. Dann der vierte grad

fraktur2/1609-Kräutterbuch-Carrichter/0028/010008.gt.txt
hat ein Schweffel / welcher allzeit vberſich
|||||||-||||||||-|||||||||||||||||||||||||||
hat ein~Schweffe~ / welcher allzeit vberſich

fraktur2/1609-Kräutterbuch-Carrichter/0028/010009.gt.txt
tringet / vnd ein ro◌ͤſche gibt durchs Blut
||||||||||||||||||||||||||||||||||||||||||
tringet / vnd ein ro◌ͤſche gibt durchs Blut

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000a.gt.txt
in die Arterien / wie das in ſeinen Exem⸗
|||||||||||||||||||||||||||||||||||||||||
in die Arterien / wie das in ſeinen Exem⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000b.gt.txt
peln folgen wird.
|||||||||||||||||
peln folgen wird.

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000c.gt.txt
Der erſte grad deß Widers ſtehet in
||||||||||||||||||||#||||||||||||||
Der erſte grad deß Wtders ſtehet in

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000d.gt.txt
der vollkommenheit / wie dann alle S~ym⸗
||||||||||||||||||||||||||||||||||||+|||
der vollkommenheit / wie dann alle Shym⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000e.gt.txt
patiſche Zeichen / Vnd dieſe Kra◌ͤuter wer⸗
#|||||||||||||||||||||||||||-|||||||||||||
gatiſche Zeichen / Vnd dieſe~Kra◌ͤuter wer⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/01000f.gt.txt
den hieher geſetzt / darumb das jhr Saltz
|||||||||||||||||||||||||||||||||||||||||
den hieher geſetzt / darumb das jhr Saltz

fraktur2/1609-Kräutterbuch-Carrichter/0028/010010.gt.txt
ſchier den Schweffel vbertringet / vnd das
||||||||||-|||||||||-|||||||||||||||||||||
ſchier den~Schweffel~vbertringet / vnd das

fraktur2/1609-Kräutterbuch-Carrichter/0028/010011.gt.txt
Saltz ſtehet im andern grad / wie das an
|||-#|||||||||||||||||||||||||||||||||||
Sal~y ſtehet im andern grad / wie das an

fraktur2/1609-Kräutterbuch-Carrichter/0028/010012.gt.txt
ſeinem orth gelernet wird werden. Der
|||||||||||||||||||||||||||||||||||||
ſeinem orth gelernet wird werden. Der

fraktur2/1609-Kräutterbuch-Carrichter/0028/010013.gt.txt
Jungfrawen im andern grad / dahin dieſe
|||||||||||||||||||||||||||||||||||||||
Jungfrawen im andern grad / dahin dieſe

fraktur2/1609-Kräutterbuch-Carrichter/0028/010014.gt.txt
Kra◌ͤuter alle geho◌ͤren / deß Saltz vnd der
||||||||||||||||||||||||||||||||||||||||||
Kra◌ͤuter alle geho◌ͤren / deß Saltz vnd der

fraktur2/1609-Kräutterbuch-Carrichter/0028/010015.gt.txt
geſaltzenen ſubſtantz halber / aber jhr ge⸗
|||||||||||||||||||||||||||||||||||||||||||
geſaltzenen ſubſtantz halber / aber jhr ge⸗

fraktur2/1609-Kräutterbuch-Carrichter/0028/010016.gt.txt
ſchmack zeucht den Schweffel an / drumb
||||||||||||||||||||||||||||-||||||||||
ſchmack zeucht den Schweffel~an / drumb

fraktur2/1609-Kräutterbuch-Carrichter/0028/010017.gt.txt
es hieher geſetzt wird.
|||||||||||||||||||||||
es hieher geſetzt wird.

fraktur2/1609-Kräutterbuch-Carrichter/0028/010018.gt.txt
Dieſe Kra◌ͤuter ſeind dem Magen im
|||||||||||||||||||||||||||||||||
Dieſe Kra◌ͤuter ſeind dem Magen im

fraktur2/1609-Kräutterbuch-Carrichter/0029/010001.gt.txt
Pulver oder Tran~k trefflich gut / dann
#|||||||||||||||+||||||||||||||||||||||
Dulver oder Tranck trefflich gut / dann

fraktur2/1609-Kräutterbuch-Carrichter/0029/010002.gt.txt
die Signatur gibt jhnen zu dem Magen
||||||||||||||||||||||||||||||||||||
die Signatur gibt jhnen zu dem Magen

fraktur2/1609-Kräutterbuch-Carrichter/0029/010003.gt.txt
den gro◌ͤſten Preiß / dann die Signatur
||||||||||||||||||||||||||||||||||||||
den gro◌ͤſten Preiß / dann die Signatur

fraktur2/1609-Kräutterbuch-Carrichter/0029/010004.gt.txt
vergleicht ſich gantz vnd gar mit einan⸗
||||||||||||||||||||||||||||||||||||||||
vergleicht ſich gantz vnd gar mit einan⸗

fraktur2/1609-Kräutterbuch-Carrichter/0029/010005.gt.txt
der / wie das an ſeinem orht verzeichnet
||||||||||||||||||||||||||||||||||||||||
der / wie das an ſeinem orht verzeichnet

fraktur2/1609-Kräutterbuch-Carrichter/0029/010006.gt.txt
wird / ſeind der Gall vnd Leber ohne ſcha⸗
||||||||||||||||||||||||||||||||||||||||||
wird / ſeind der Gall vnd Leber ohne ſcha⸗

fraktur2/1609-Kräutterbuch-Carrichter/0029/010007.gt.txt
den / doch mit jhrem Natu◌ͤrlichen zuſatz /
||||||||||||||||||||||||||||||||||||||||||
den / doch mit jhrem Natu◌ͤrlichen zuſatz /

fraktur2/1609-Kräutterbuch-Carrichter/0029/010008.gt.txt
auß dem erſten grad deß Waſſermans vñ
|||||||||||||||||||||||||||||||||||||
auß dem erſten grad deß Waſſermans vñ

fraktur2/1609-Kräutterbuch-Carrichter/0029/010009.gt.txt
Jungfraw / wie das auff der Figur ſtehet /
||||||||||||||||||||||||||||||||||||||||||
Jungfraw / wie das auff der Figur ſtehet /

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000a.gt.txt
So ſind ſie dem Miltz fu◌ͤrtrefflich gut /
|||||||||||||||||||||||||||||||||||||||||
So ſind ſie dem Miltz fu◌ͤrtrefflich gut /

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000b.gt.txt
lo◌ͤſchen die Gall / geben dem Magen da◌ͤw⸗
|||||||||||||||||||||||||||||||||||||||||
lo◌ͤſchen die Gall / geben dem Magen da◌ͤw⸗

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000c.gt.txt
ung / jhrer weiſſen ſubſtantz halben / Jhrer
||||||||||||||||||||||||||||||||||||||||||||
ung / jhrer weiſſen ſubſtantz halben / Jhrer

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000d.gt.txt
Schweffeliſchen ſubſtantz halber reinigen
|||||||||||||||||||||||||||||||||||||||||
Schweffeliſchen ſubſtantz halber reinigen

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000e.gt.txt
ſie die Venas Mele~nterij, Jhres ſaltz hal⸗
||||||||#|||||-|#|+#|||###|||||||||||||||||
ſie die /enas ~eiem terni. Jhres ſaltz hal⸗

fraktur2/1609-Kräutterbuch-Carrichter/0029/01000f.gt.txt
ben tru◌ͤcknen ſie das Miltz / vnd das auff
||||||||||||||||||||||||||||||||||||||||||
ben tru◌ͤcknen ſie das Miltz / vnd das auff

fraktur2/1609-Kräutterbuch-Carrichter/0029/010010.gt.txt
zweyerley art / durch das Saltz vnd zwey
||#|||||||||||||||||||||||||||||||||||||
zwdyerley art / durch das Saltz vnd zwey

fraktur2/1609-Kräutterbuch-Carrichter/0029/010011.gt.txt
theil / vnd ein grad Schweffels / Faule
|||||||||||||||||||||||||||||||||||||||
theil / vnd ein grad Schweffels / Faule

fraktur2/1609-Kräutterbuch-Carrichter/0029/010012.gt.txt
da◌ͤmpff / welche vberſich ſteigen / verzehren
|||||||||||||||||||||||||||||||||||||||||||||
da◌ͤmpff / welche vberſich ſteigen / verzehren

fraktur2/1609-Kräutterbuch-Carrichter/0029/010013.gt.txt
ſie / vnd halten ſie vnderſich.
|||||||||||||||||||||||||||||||
ſie / vnd halten ſie vnderſich.

