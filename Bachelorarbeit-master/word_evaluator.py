import re
import sys
from collections import defaultdict


# names of features
capital_letter = 'capital_letter'
long_word = 'long_word'
max_probability = 'max_probability'
min_probability = 'min_probability'
words_equal = "words_equal"


def read_3grams(file_path):
    # open profile
    f = open(file_path, 'r', encoding='utf-8')
    # read first line
    line = f.readline()

    corpus_size = 0
    wort_frequency_dict = {}

    while line:
        # get 3gram and its frequency from current line
        line_elements = line.split('\t')
        frequency = int(line_elements[0])
        trigram = re.sub('\n', '', line_elements[1])
        # add 3gram and its frequency to the dictionary
        wort_frequency_dict[trigram] = frequency
        # add frequency to the corpus size
        corpus_size = corpus_size + frequency

        # read next line
        line = f.readline()

    result = [corpus_size, wort_frequency_dict]
    return result

def read_suggestion(file_path, top_n = 5):

    suggestion_dict = {}

    current_word = ""
    current_suggestions = []

    ocr_token_freq = defaultdict(int)

    # In this special case, do not use Python's list comprehension
    # It would require > 8GB of RAM for combined fraktur file
    # Instead read file line-by-line which is much slower
    # with open(file_path, 'r', encoding='utf-8') as f:
    # lines = [line.rstrip() for line in f.readlines()]

    f = open(file_path, 'r', encoding='utf-8')

    line = f.readline()

    while line:
        if not line.startswith("@") and not line.startswith("profiler: caught"):
            # E.g. Hainiſch:{hainisch+[(s:ſ,5)]}+ocr[(a:im,1)],voteWeight=0.983226,levDistance=2,dict=dict_modern_hypothetic_errors

            word = line.split(":")[0]

            fields = line.split("]")

            features = fields[2].split(",")[1:]

            feature_dict = { item.split("=")[0]: item.split("=")[1] for item in features }

            vote_weight = float(feature_dict["voteWeight"])
            lev_distance = float(feature_dict["levDistance"])

            current_suggestions.append({word : [vote_weight, lev_distance]})
        elif line.startswith("profiler: caught"):
            # Error in input file
            print("Error in line:", line)
        else:
            if not current_word == "":
                ocr_token_freq[current_word] += 1

                suggestion_dict[current_word] = current_suggestions[:top_n]

                current_word = line.split(":")[0]
                del current_suggestions
                current_suggestions = []
            else:
                # @Kraͤfftig:Kraͤfftig
                current_word = line.split(":")[0]

        if not current_word == "" and len(current_suggestions) > 0:
            suggestion_dict[current_word] = current_suggestions[:top_n]

        line = f.readline()

    return suggestion_dict, ocr_token_freq

def split_word_into_trigrams(word):
    """Return list of trigrams of the given word"""
    # add $$ to the word
    word = '$' + word + '$'

    trigrams = []
    for i in range(0, len(word) - 3 + 1):
        trigrams.append(word[i : i + 3])
    return trigrams


def find_min_and_max_probabilities(word, corpus_size, word_frequency_dict):
    """Return min and max probabilities for the given word"""
    min_trigram = ''
    max_trigram = ''

    min_probability = sys.maxsize
    max_probability = -sys.maxsize - 1

    # split word into trigrams
    trigrams = split_word_into_trigrams(word)

    for trigram in trigrams:
        # get frequency of the current trigram
        try:
            frequency = word_frequency_dict[trigram]
        except KeyError:
            frequency = 0
        # calculate probability of the current trigram
        probability = frequency / corpus_size

        # check if this is the min probability
        if probability < min_probability:
            min_probability = probability
            min_trigram = trigram

        # check if this is the min probability
        if probability > max_probability:
            max_probability = probability
            max_trigram = trigram

    # add min and max 3grams and their probabilities in result
    min_and_max_probabilities = dict()
    min_and_max_probabilities['min'] = [min_trigram, min_probability]
    min_and_max_probabilities['max'] = [max_trigram, max_probability]

    return min_and_max_probabilities



def calculate_features(profile_path, output_path, corpus_size, wort_frequency_dict, suggestion_dict, ocr_token_freq, suggestions_top_n = 5):
    """Calculate features for each word and write it into output file"""

    # open profile
    profile = open(profile_path, 'r', encoding='utf-8')
    # read first line
    line = profile.readline()

    line_number = 1     # for debugging

    # create output file
    output_file = open(output_path, 'w', encoding='utf-8')

    while line:

        # no features for words without @
        if not line.startswith('@'):
            # read next line
            line = profile.readline()
            line_number = line_number + 1
            # skip current iteration
            continue

        # get 2 words from line
        words_in_line = line.split(':')
        word_with_at = re.sub('@', '', words_in_line[0])
        word_without_at = re.sub('\n', '', words_in_line[1])

        # if there is no word after @ than skip
        if len(word_with_at) < 1:
            # read next line
            line = profile.readline()
            line_number = line_number + 1
            # skip current iteration
            continue

        # features (capital_letter, long_word, max_probability, min_probability, words_equal)
        features = []

        # calculate feature capital_letter
        if word_with_at[0].islower():
            features.append(0)
        else:
            features.append(1)

        # calculate feature long_word
        if len(word_with_at) < 5:
            features.append(0)
        else:
            features.append(1)

        # calculate features min and max probabilities
        min_and_max = find_min_and_max_probabilities(word_with_at, corpus_size, wort_frequency_dict)
        features.append(str(min_and_max['min'][1]) + " (" + min_and_max['min'][0] + ")")
        features.append(str(min_and_max['max'][1]) + " (" + min_and_max['max'][0] + ")")

        # calculate feature words_equal
        if word_with_at != word_without_at:
            features.append(0)
        else:
            features.append(1)

        vote_weight_features  = []
        lev_distance_features = []

        if words_in_line[0] in suggestion_dict and len(suggestion_dict[words_in_line[0]]) > 0:
            suggestions = suggestion_dict[words_in_line[0]]
            len_suggestions = len(suggestions)

            sum_vote_weight = 0.0
            sum_lev_distance = 0

            for suggestion in suggestions:
                for item in suggestion.keys():
                    sum_vote_weight  += suggestion[item][0]
                    sum_lev_distance += suggestion[item][1]

                    vote_weight_features.append(float(suggestion[item][0]))
                    lev_distance_features.append(float(suggestion[item][1]))

            # Average vote weight
            features.append(float(sum_vote_weight / len_suggestions))
            features.append(float(sum_lev_distance / len_suggestions))
        else:
            # Pad features
            features.append(0.0)
            features.append(0.0)

        # Pad vote_weight and lev_distance feature vector to suggestions_top_n
        # E.g. l = [1.0, 2.3] and suggestions_top_n = 5
        # l needs to be padded with 0.0 to be a valid feature vector:
        # l += [0.0] * (5 - len(l))
        # Results in: [1.0, 2.3, 0.0, 0.0, 0.0]
        vote_weight_features  += [0.0] * (5 - len(vote_weight_features))
        lev_distance_features += [0.0] * (5 - len(lev_distance_features))

        feat = ["capital_letter", "long_word", "max_probability",
                "min_probability", "words_equal", "vote_weight_avg",
                "lev_distance_avg"]

        # Append vote_weight and lev_distance features
        def append_feature(feature_name, features_list):
            for i in range(0, suggestions_top_n):
                feat.append(feature_name + str(i))
                features.append(features_list[i])

        append_feature("vote_weight_", vote_weight_features)
        append_feature("lev_distance_", lev_distance_features)

        feat.append("ocr_token_exists")

        # word exists in ocr_token_freq
        ocr_token_exists = 0.0

        # Ocr token exists more than one times (new feature)
        if words_in_line[0] in ocr_token_freq and \
           ocr_token_freq[words_in_line[0]] > 1:
            ocr_token_exists = 1.0

        features.append(ocr_token_exists)

        # format features to string
        result = word_with_at + "   "
        for i in range(0,len(features)):
            result = result + feat[i] + " = " + str(features[i]) + ", "
        result = result + '\n'

        # write word and calculated features into output file
        output_file.flush()
        output_file.write(result)

        # read next line
        line = profile.readline()
        line_number = line_number + 1

    # close files
    profile.close()
    output_file.close()
