import re
import sys
import numpy as np
import time

from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold
from sklearn.metrics import precision_recall_fscore_support

with open("features.txt", "r", encoding='utf-8') as f:
    lines = [line.rstrip() for line in f.readlines()]

start_time = time.time()


word_equal_regex       = re.compile(r'.*words_equal = ([01]).*')
long_word_regex        = re.compile(r'.*long_word = ([01]).*')
min_prob_regex         = re.compile(r'.*min_probability = ([0-9\.e\-]+).*')
capital_letter_regex   = re.compile(r'.*capital_letter = ([01]).*')
max_prob_regex         = re.compile(r'.*max_probability = ([0-9e\.\-]+).*')
vote_weight_regex      = re.compile(r'.*vote_weight_avg = ([0-9e\.\-]+).*')
lev_distance_regex     = re.compile(r'.*lev_distance_avg = ([0-9]).*')
vote_weight_0_regex    = re.compile(r'.*vote_weight_0 = ([0-9e\.\-]+).*')
vote_weight_1_regex    = re.compile(r'.*vote_weight_1 = ([0-9e\.\-]+).*')
vote_weight_2_regex    = re.compile(r'.*vote_weight_2 = ([0-9e\.\-]+).*')
vote_weight_3_regex    = re.compile(r'.*vote_weight_3 = ([0-9e\.\-]+).*')
vote_weight_4_regex    = re.compile(r'.*vote_weight_4 = ([0-9e\.\-]+).*')
lev_distance_0_regex   = re.compile(r'.*lev_distance_0 = ([0-9]).*')
lev_distance_1_regex   = re.compile(r'.*lev_distance_1 = ([0-9]).*')
lev_distance_2_regex   = re.compile(r'.*lev_distance_2 = ([0-9]).*')
lev_distance_3_regex   = re.compile(r'.*lev_distance_3 = ([0-9]).*')
lev_distance_4_regex   = re.compile(r'.*lev_distance_4 = ([0-9]).*')
ocr_token_exists_regex = re.compile(r'.*ocr_token_exists = ([0-9]).*')

# Features: numpy-Array
X = []

# Labels: numpy-Array (consists of 1s and 0s)
y = []

# Debug = True shows some error statistics
debug = False

for line in lines:
    word_equal_match       = re.match(word_equal_regex, line)
    long_word_match        = re.match(long_word_regex, line)
    min_prob_match         = re.match(min_prob_regex, line)
    capital_letter_match   = re.match(capital_letter_regex, line)
    max_prob_match         = re.match(max_prob_regex, line)
    vote_weight_match      = re.match(vote_weight_regex, line)
    lev_distance_match     = re.match(lev_distance_regex, line)
    vote_weight_0_match    = re.match(vote_weight_0_regex, line)
    vote_weight_1_match    = re.match(vote_weight_1_regex, line)
    vote_weight_2_match    = re.match(vote_weight_2_regex, line)
    vote_weight_3_match    = re.match(vote_weight_3_regex, line)
    vote_weight_4_match    = re.match(vote_weight_4_regex, line)
    lev_distance_0_match   = re.match(lev_distance_0_regex, line)
    lev_distance_1_match   = re.match(lev_distance_1_regex, line)
    lev_distance_2_match   = re.match(lev_distance_2_regex, line)
    lev_distance_3_match   = re.match(lev_distance_3_regex, line)
    lev_distance_4_match   = re.match(lev_distance_4_regex, line)
    ocr_token_exists_match = re.match(ocr_token_exists_regex, line)

    if not (word_equal_match and long_word_match and
            min_prob_match and capital_letter_match and
            max_prob_match and vote_weight_match and lev_distance_match and
            vote_weight_0_match and vote_weight_1_match and
            vote_weight_2_match and vote_weight_3_match and
            vote_weight_4_match  and
            lev_distance_0_match and lev_distance_1_match and
            lev_distance_2_match and lev_distance_3_match and
            lev_distance_4_match) and ocr_token_exists_match:
        print("No match:", line)
    else:
        word = line.split(" ")[0]

        # Features to be used for training
        # Just comment out unnecessary features for testing/debugging
        features = np.asarray([float(long_word_match.group(1)),      #])
                               float(min_prob_match.group(1)),      #])
                               float(capital_letter_match.group(1)), #])
                               float(max_prob_match.group(1)),      #])
                               float(vote_weight_match.group(1)),   #])
                               float(lev_distance_match.group(1)),  #])
                               float(ocr_token_exists_match.group(1)),#])
                               float(vote_weight_0_match.group(1)), #])
                               float(vote_weight_1_match.group(1)), #])
                               float(vote_weight_2_match.group(1)), #])
                               float(vote_weight_3_match.group(1)), #])
                               float(vote_weight_4_match.group(1)), #])
                               float(lev_distance_0_match.group(1)), #])
                               float(lev_distance_1_match.group(1)),  #])
                               float(lev_distance_2_match.group(1)),  #])
                               float(lev_distance_3_match.group(1)),  #])
                               float(lev_distance_4_match.group(1)),])

        X.append(features)

        y.append(int(word_equal_match.group(1)))

def perform_classification(X, y, n_fold, classifier_name):
    kf = KFold(n_splits=n_fold)

    print(kf.get_n_splits(X))

    sum_precision = 0.0
    sum_recall    = 0.0
    sum_fscore    = 0.0
    sum_accuracy  = 0.0

    for train_index, test_index in kf.split(X):
        print("Train:", train_index, "Test:", test_index)

        correct_classified = 0

        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        if classifier_name == "log_reg":
            classifier  = LogisticRegression(verbose = 0)
        elif classifier_name == "svm":
            classifier = LinearSVC(random_state=0, verbose=1)

        classifier.fit(X_train, y_train)

        y_score_test = classifier.predict(X_test)

        p, r, f, _ = precision_recall_fscore_support(y_test, y_score_test,
                                                     average='binary')

        for result in zip(y_test, y_score_test):
            gold, prediction = result
            if gold == prediction:
                correct_classified += 1

        accuracy = float(correct_classified / len(y_test))

        if debug:
            for i, item in enumerate(zip(y_test, y_score_test)):
                gold, prediction = item

                if gold != prediction:

                    if int(gold) == 0 and int(prediction) == 1:
                        print("Gold:", gold, "Prediction:", prediction, "Test index:", test_index[0] + i)
                        print("Test data:", lines[test_index[0] + i])

        sum_precision += p
        sum_recall    += r
        sum_fscore    += f
        sum_accuracy  += accuracy

        print("Precision:", p)
        print("Recall:", r)
        print("Accuracy:", accuracy)
        print("F1-Score:", f)

    print("Averaged Precision:", sum_precision / n_fold)
    print("Averaged Recall:",    sum_recall    / n_fold)
    print("Averaged Accuracy:",  sum_accuracy  / n_fold)
    print("Averaged F-Score:",   sum_fscore    / n_fold)

n_fold = 10

X = np.asarray(X)
y = np.asarray(y)

if len(sys.argv) > 1:
    classifier = sys.argv[1]
else:
    classifier = "log_reg"

perform_classification(X, y, n_fold, classifier)
print("Time (seconds):", time.time() - start_time)