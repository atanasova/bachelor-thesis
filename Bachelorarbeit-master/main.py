import word_evaluator

# profiles
path_1487 = 'profile/1487-GartDerGesunthait.txt'
path_1532_tallat = 'profile/1532-ArtzneyBuchleinDerKreutter-Tallat.txt'
path_1532_brunfels = 'profile/1532-ContrafaytKreüterbuch-Brunfels.txt'
path_1543 = 'profile/1543-NewKreüterbuch-Fuchs.txt'
path_1557 = 'profile/1557-WieSichMeniglich-Bodenstein.txt'
path_1588 = 'profile/1588-Paradeißgärtlein-Rosbach.txt'
path_1603 = 'profile/1603-AlchymistischePractic-Libavius.txt'
path_1609_durante = 'profile/1609-HortulusSanitatis-Durante.txt'
path_1609_carrichter = 'profile/1609-Kräutterbuch-Carrichter.txt'
path_1639 = 'profile/1639-PflantzGart-Rhagor.txt'
path_1652 = 'profile/1652-WundArtzney-Fabricius.txt'
path_1673 = 'profile/1673-ThesaurusSanitatis-Nasser.txt'
path_1675 = 'profile/1675-CurioserBotanicus-NN.txt'
path_1687 = 'profile/1687-DerSchweitzerischeBotanicus-Roll.txt'
path_1722 = 'profile/1722-FloraSaturnizans-Henckel.txt'
path_1735 = 'profile/1735-MysterivmSigillorvm-Hiebner.txt'
path_1764 = 'profile/1764-EinleitungZuDerKräuterkenntniß-Oeder.txt'
path_1774 = 'profile/1774-Unterricht-Eisen.txt'
path_1828 = 'profile/1828-DieEigenschaftenAllerHeilpflanzen-NN.txt'
path_1870 = 'profile/1870-DeutschePflanzennamen-Graßmann.txt'

path_all  = 'fraktur1und2/beide'

# file with given frequencies of 3grams
path_trigrams = 'dta-3grams-sorted.txt'

# output file
path_output = 'features.txt'

# parse frequencies of 3grams and calculate corpus size
content = word_evaluator.read_3grams(path_trigrams)
corpus_size = content[0]
wort_frequency_dict = content[1]

suggestion_dict, ocr_token_freq = word_evaluator.read_suggestion(path_all)

# calculate features
word_evaluator.calculate_features(path_all, path_output, corpus_size, wort_frequency_dict, suggestion_dict, ocr_token_freq)
